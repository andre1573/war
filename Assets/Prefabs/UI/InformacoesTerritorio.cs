using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Pun;


[RequireComponent(typeof(PhotonView))]
public class InformacoesTerritorio : MonoBehaviourPunCallbacks
{
    [SerializeField] public TMP_Text textNome, textQtdExercitos, textQtdTanques;
    [SerializeField] public GameObject objTanque, marcacaoFronteiras;
    [SerializeField] public Image imgSoldado, imgTanque, imgBaseTiroSoldados;
    [SerializeField] public Animator animTiroSoldado, animTiroTanque, animTiroMissil, animDefesaConquista;
    [SerializeField] SoundsController soundsController;
    [SerializeField] public DirecionarImagemParaAlvo direcionarImagemParaAlvoController;
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        objTanque.SetActive(false);
    }

    [PunRPC]
    void RPC_AtivarAnimacaoSoldadoAtacando()
    {
        animTiroSoldado.SetTrigger("soldadoatacando");
        soundsController.TocarSoundTiroSoldado();
    }

    [PunRPC]
    void RPC_AtivarAnimacaoTanqueAtacando()
    {
        animTiroTanque.SetTrigger("tanqueatacando");
        soundsController.TocarSoundTiroTanque();
    }

    [PunRPC]
    void RPC_AtivarAnimacaoDefendendoAtaque()
    {
        animDefesaConquista.SetTrigger("defendendo");
    }

    [PunRPC]
    void RPC_AtivarAnimacaoRecebendoMissil(bool ataqueVenceuReponse)
    {
        if (ataqueVenceuReponse)
        {
            animTiroMissil.SetTrigger("missilatacando");
            soundsController.TocarSoundTiroMissil();
        }
        else soundsController.TocarSoundMissilFalhou();
    }

    [PunRPC]
    void RPC_AtivarAnimacaoTanqueFalhou()
    {
        animTiroMissil.SetTrigger("missilatacando");
        soundsController.TocarSoundTanqueFalhou();

    }

    [PunRPC]
    void RPC_AtivarAnimacaoConquistado()
    {
        animDefesaConquista.SetTrigger("conquistado");
        soundsController.TocarSoundConquistado();
    }

    public void AtivarAnimacaoAtacando(ModalAdicionarExercitos.TipoAbaArmamentos tipoAbaAtaqueResponse)
    {
        if (ModalAdicionarExercitos.TipoAbaArmamentos.Soldados.Equals(tipoAbaAtaqueResponse))
        {
            PV.RPC("RPC_AtivarAnimacaoSoldadoAtacando", RpcTarget.All);
        }
        else if (ModalAdicionarExercitos.TipoAbaArmamentos.Tanques.Equals(tipoAbaAtaqueResponse))
        {
            PV.RPC("RPC_AtivarAnimacaoTanqueAtacando", RpcTarget.All);
        }
    }

    public void AtivarAnimacaoDefendendoAtaque()
    {
        PV.RPC("RPC_AtivarAnimacaoDefendendoAtaque", RpcTarget.All);
    }

    public void AtivarAnimacaoTanqueFalhou()
    {
        PV.RPC("RPC_AtivarAnimacaoTanqueFalhou", RpcTarget.All);
    }

    public void AtivarAnimacaoRecebendoMissil(bool ataqueVenceuResponse)
    {
        PV.RPC("RPC_AtivarAnimacaoRecebendoMissil", RpcTarget.All, ataqueVenceuResponse);
    }

    public void AtivarAnimacaoConquistado()
    {
        PV.RPC("RPC_AtivarAnimacaoConquistado", RpcTarget.All);
    }

}
