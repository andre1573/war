using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistaCheck : MonoBehaviour
{

    PistasBunnyhopControle pistaControle;
    bool jaPassou = false;

    private void OnTriggerEnter(Collider other)
    {
        if (jaPassou) return;
        if(other.tag == "Player")
        {
            jaPassou = true;
            setarPistaControle();
            pistaControle.CriarNovaPista();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        jaPassou = false;
        setarPistaControle();
    }

    // Update is called once per frame
    void Update()
    {
        setarPistaControle();
    }

    private void setarPistaControle()
    {
        if (pistaControle == null)
        {
            GameObject objPistaControle = GameObject.FindGameObjectWithTag("pistabunnyhopcontrole");
            if (objPistaControle != null)
            {
                pistaControle = objPistaControle.GetComponent<PistasBunnyhopControle>();
            }
        }
    }

}
