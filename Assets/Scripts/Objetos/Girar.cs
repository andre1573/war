using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView), typeof(PhotonTransformView))]
public class Girar : MonoBehaviourPunCallbacks
{

    public float velocidade = 1f;
    public string angulo = "x";
    public bool porTempo = false, porAnimation = false;
    public float tempoAtivar = 5.0f, tempoDesativar = 1.0f;
    public float contador = 0;
    private bool ativo = false;
    public string abrindo, fechando;
    PhotonView PV;
    Animation anim;

    private void Start()
    {
        PV = GetComponent<PhotonView>();
        anim = GetComponent<Animation>();

    }
    // Update is called once per frame
    void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if (porTempo)
        {
            if (!ativo)
            {
                if (contador > tempoAtivar)
                {
                    if (porAnimation)
                    {
                        anim.Play(abrindo);
                    }
                    else
                    {
                        if (angulo == "x")
                        {
                            transform.Rotate(velocidade * Time.deltaTime, 0, 0, Space.Self);
                        }
                        else if (angulo == "y")
                        {
                            transform.Rotate(0, velocidade * Time.deltaTime, 0, Space.Self);
                        }
                        else if (angulo == "z")
                        {
                            transform.Rotate(0, 0, velocidade * Time.deltaTime, Space.Self);
                        }
                    }
                    contador = 0;
                    ativo = true;
                }
                else
                {
                    contador += Time.deltaTime;
                }
            }
            else
            {
                if (contador > tempoDesativar)
                {

                    if (porAnimation)
                    {
                        anim.Play(fechando);
                    }
                    else
                    {
                        if (angulo == "x")
                        {
                            transform.Rotate(-velocidade * Time.deltaTime, 0, 0, Space.Self);
                        }
                        else if (angulo == "y")
                        {
                            transform.Rotate(0, -velocidade * Time.deltaTime, 0, Space.Self);
                        }
                        else if (angulo == "z")
                        {
                            transform.Rotate(0, 0, -velocidade * Time.deltaTime, Space.Self);
                        }
                    }
                    contador = 0;
                    ativo = false;
                }
                else
                {
                    contador += Time.deltaTime;
                }
            }
        }
        else
        {
            if (angulo == "x")
            {
                transform.Rotate(velocidade * Time.deltaTime, 0, 0, Space.Self);
            }
            else if (angulo == "y")
            {
                transform.Rotate(0, velocidade * Time.deltaTime, 0, Space.Self);
            }
            else if (angulo == "z")
            {
                transform.Rotate(0, 0, velocidade * Time.deltaTime, Space.Self);
            }
        }
    }
}
