using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaoAssassino : MonoBehaviour
{

    [SerializeField] bool inModoBunnyhop = false, isBaseChaoAssassinoMove = false;
    [SerializeField] PistasBunnyhopControle pistasBunnyhopControle;
    [SerializeField] GameObject baseChaoAssassino;
    private GameObject player;


    void OnTriggerEnter(Collider other)
    {
        if (isBaseChaoAssassinoMove) return;
        if(other.gameObject.tag == "Player")
        {
            player = null;
            other.gameObject.GetComponent<PlayerController>().Die();
        }
    }

    void Start()
    {
        gameObject.tag = "chaoassassino";
        if (isBaseChaoAssassinoMove) return;
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    private void Update()
    {
        if (!isBaseChaoAssassinoMove) return;
        if (inModoBunnyhop)
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + (0.2f * Time.deltaTime), player.transform.position.z);
            }
        }
    }

    public void ReiniciarPosicaoBaseChaoAssassino()
    {
        if (baseChaoAssassino != null)
        {
            baseChaoAssassino.transform.position = new Vector3(baseChaoAssassino.transform.position.x, -7.6f, 0);
        }
    }

}
