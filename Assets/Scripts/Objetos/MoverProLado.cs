using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView), typeof(PhotonTransformView))]
public class MoverProLado : MonoBehaviourPunCallbacks
{

    public float min = 0, max = 0, velocidade = 0.05f;
    public string angulo = "x";
    public bool lado = true;
    PhotonView PV;

    private void Start()
    {
        PV = GetComponent<PhotonView>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if (angulo == "x")
        {
            if (lado)
            {
                if (transform.localPosition.x <= max)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x + velocidade*Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
                }
                else
                {
                    lado = !lado;
                }
            }
            else
            {
                if (transform.localPosition.x >= min)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x - velocidade * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
                }
                else
                {
                    lado = !lado;
                }
            }
        }
        else if (angulo == "y")
        {
            if (lado)
            {
                if (transform.localPosition.y <= max)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + velocidade * Time.deltaTime, transform.localPosition.z);
                }
                else
                {
                    lado = !lado;
                }
            }
            else
            {
                if (transform.localPosition.y >= min)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - velocidade * Time.deltaTime, transform.localPosition.z);
                }
                else
                {
                    lado = !lado;
                }
            }
        }
        else if (angulo == "z")
        {
            if (lado)
            {
                if (transform.localPosition.z <= max)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + velocidade * Time.deltaTime);
                }
                else
                {
                    lado = !lado;
                }
            }
            else
            {
                if (transform.localPosition.z >= min)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - velocidade * Time.deltaTime);
                }
                else
                {
                    lado = !lado;
                }
            }
        }
    }
}
