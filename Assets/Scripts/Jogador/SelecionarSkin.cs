using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SelecionarSkin : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] ReferenciaSkins[] skins;
    PhotonView PV;
    int index = 0;

    void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    void Start()
    {
        if (PV.IsMine)
        {
            iniciarMinhaSkin();
        }
    }

    public void iniciarMinhaSkin()
    {
        index = int.Parse(PlayerPrefs.GetString("personagemskin"));
        if(index >= 0)
        {
            skins[index].ativarObjetosDaSkin();
        }
        else
        {
            skins[0].ativarObjetosDaSkin();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(index);
        }
        else
        {
            skins[(int)stream.ReceiveNext()].ativarObjetosDaSkin();
        }
    }

}
