using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Leguar.HiddenVars;

public class PlayerController : MonoBehaviourPunCallbacks
{
	PhotonView PV;
	[HideInInspector] public PlayerManager playerManager;
	[HideInInspector] public HiddenVars hiddenVars;

	void Awake()
	{
		hiddenVars = new HiddenVars();
		PV = GetComponent<PhotonView>();
		playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>();
	}

	void Start()
	{
		if (PV.IsMine)
		{
			Hashtable hash = new Hashtable();
			hash.Add("personagemskin", PlayerPrefs.GetInt("personagemskin"));
			PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
			resetarCaracteristicasPlayer();
		}
	}

	void Update()
	{
		if (!PV.IsMine) return;
	}

	public void Die()
	{
		playerManager.Die();
	}

	public void resetarCaracteristicasPlayer()
    {
	}

}