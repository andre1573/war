using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EmpurrarJogador : MonoBehaviourPunCallbacks
{

    public GameObject playerObj;
    PhotonView PV;
    public AudioSource kickAir, kickPlayer;
    private float timeLeft = 0.5f;

    private void Start()
    {
        PV = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!PV.IsMine) return;
        if (Input.GetButtonDown("Fire1") && timeLeft <= 0)
        {
            timeLeft = 0.5f;
            playerObj.GetComponent<Animator>().SetBool("empurrando", true);
            bool achouInimigo = false;
            foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if(player.transform.GetComponent<PhotonView>().Owner.UserId != PV.Owner.UserId)
                {
                    achouInimigo = true;
                    float distance = Vector3.Distance(transform.position, player.transform.position);
                    Debug.Log("Distancia do player: " + distance);
                    if (distance <= 1.1)
                    {
                        playerObj.GetComponent<PlayerController>().hiddenVars.SetFloat("velBunnyhop", playerObj.GetComponent<PlayerController>().hiddenVars.GetFloat("velBunnyhop") / 1.1f);
                        player.transform.GetComponent<PhotonView>().RPC("RPC_Empurrao", RpcTarget.All, transform.forward, 5.0f);
                        PV.RPC("RPC_AcoesEmpurrao", RpcTarget.All, true);
                        Debug.Log("EMPURROU PLAYER");
                    }
                    else
                    {
                        PV.RPC("RPC_AcoesEmpurrao", RpcTarget.All, false);
                    }
                }
            }
            if(!achouInimigo) PV.RPC("RPC_AcoesEmpurrao", RpcTarget.All, false);
        }
        else
        {
            playerObj.GetComponent<Animator>().SetBool("empurrando", false);
        }
        timeLeft -= Time.deltaTime;
    }

    [PunRPC]
    void RPC_AcoesEmpurrao(bool acertou)
    {
        if(acertou) kickPlayer.Play();
        else kickAir.Play();
    }

}
