using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiscarImagem : MonoBehaviour
{
    [SerializeField][HideInInspector] RawImage image;
    public float blinkInterval = 1f;

    private float timeSinceLastBlink;
    private bool isImageVisible = true;

    private void Awake()
    {
        image = GetComponent<RawImage>();
    }

    void Update()
    {
        timeSinceLastBlink += Time.deltaTime;
        if (timeSinceLastBlink >= blinkInterval)
        {
            timeSinceLastBlink = 0;
            isImageVisible = !isImageVisible;
            image.enabled = isImageVisible;
        }
    }
}
