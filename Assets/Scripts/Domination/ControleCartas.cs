using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class ControleCartas : MonoBehaviourPunCallbacks
{

    [SerializeField] public GameControllerDomination gameController;
    [SerializeField] public ControleObjetivos objetivosController;
    [SerializeField] public GameObject panelCartas, panelBotoesCartas;
    [SerializeField] public GameObject cartaPrefab;
    [SerializeField] public Transform cartasContent;
    [SerializeField] public TMP_Text textQtdCartas;
    [SerializeField] [HideInInspector] public List<Carta> cartasSelecionadasParaTrocar;
    [SerializeField] public TrocaCartas trocaCartasAnim;
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        cartasSelecionadasParaTrocar = new List<Carta>();
    }

    public void AbrirPanelCartas()
    {
        if (panelCartas == null) return;
        if (panelCartas.activeSelf)
        {
            this.FecharPanelCartas();
        }
        else
        {
            if (objetivosController.buttonFecharCardObjetivo.activeSelf) objetivosController.FecharCardObjetivo(); 
            panelCartas.SetActive(true);
        }
    }

    public void FecharPanelCartas()
    {
        if (panelCartas == null) return;
        panelCartas.SetActive(false);
    }

    public void adicionarCartaNaListaParaTrocar(Carta cartaResponse) {
        cartasSelecionadasParaTrocar.Add(cartaResponse);
    }

    public void removerCartaDaListaParaTrocar(Carta cartaResponse)
    {
        cartasSelecionadasParaTrocar.Remove(cartaResponse);
    }

    public void esvaziarListaCartasSelecionadas()
    {
        foreach (Carta carta in cartasSelecionadasParaTrocar.ToArray())
        {
            carta.DeselecionarCartaParaTrocar();
        }
    }

    public void EfetuarDescarteDasCartasSelecionadas()
    {
        foreach (Carta carta in cartasSelecionadasParaTrocar)
        {
            destruirInstancia(carta.dono, carta);
        }
        esvaziarListaCartasSelecionadas();
        PV.RPC("RPC_AtualizarInfoCartas", RpcTarget.All, gameController.meuIndex, gameController.jogadores[gameController.meuIndex].cartas.Count);
    }

    public void EfetuarTrocaDeCartasSelecionadas()
    {
        if(cartasSelecionadasParaTrocar.Count > 0)
        {
            if (verificarSeExisteCartaDoTipoExercito())
            {
                efetuarTrocaDeCartas();        
            }
            else
            {
                gameController.textMensagem.text = "� Necess�rio selecionar pelo menos uma carta do tipo ex�rcito";
            }
        }
        else
        {
            gameController.textMensagem.text = "Selecione pelo menos uma carta";
        }
    }

    private bool verificarSeExisteCartaDoTipoExercito()
    {
        foreach(Carta carta in cartasSelecionadasParaTrocar)
        {
            if (carta.tipoCarta != Carta.TipoCarta.MULTIPLICADOR)
            {
                return true;
            }
        }
        return false;
    }

    private void efetuarTrocaDeCartas()
    {
        int valorMultiplicador = 0;
        int valorSoldados = 0;
        int valorTanques = 0;
        int valorMissil = 0;
        int[] arrayTipo = new int[cartasSelecionadasParaTrocar.Count];
        int[] arrayValor = new int[cartasSelecionadasParaTrocar.Count];

        for(int i=0; i < cartasSelecionadasParaTrocar.Count; i++)
        {
            Carta carta = cartasSelecionadasParaTrocar[i];
            if (carta.tipoCarta == Carta.TipoCarta.MULTIPLICADOR)
            {
                valorMultiplicador += carta.valor; 
            }
            else if (carta.tipoCarta == Carta.TipoCarta.SOLDADO)
            {
                valorSoldados += carta.valor;
            }
            else if (carta.tipoCarta == Carta.TipoCarta.TANQUE)
            {
                valorTanques += carta.valor;
            }
            else if (carta.tipoCarta == Carta.TipoCarta.MISSIL)
            {
                valorMissil += carta.valor;
            }
            arrayTipo[i] = (int)carta.tipoCarta;
            arrayValor[i] = carta.valor;
            destruirInstancia(carta.dono, carta);
        }
        PV.RPC("RPC_TrocaCartaAnimacao", RpcTarget.All, gameController.meuIndex, arrayTipo, arrayValor);
        esvaziarListaCartasSelecionadas();
        if (valorMultiplicador == 0) valorMultiplicador = 1;
        gameController.jogadores[gameController.meuIndex].AcrescentarSoldados(valorSoldados * valorMultiplicador);
        gameController.jogadores[gameController.meuIndex].AcrescentarTanques(valorTanques * valorMultiplicador);
        gameController.jogadores[gameController.meuIndex].AcrescentarMisseis(valorMissil * valorMultiplicador);
        gameController.modalAdicionarExercitos.atualizarModalAdicionarExercitos();
        PV.RPC("RPC_AtualizarInfoCartas", RpcTarget.All, gameController.meuIndex, gameController.jogadores[gameController.meuIndex].cartas.Count);
        if(valorTanques > 0 || valorMissil > 0)
        {
            AtualizarInfoArmamentos(gameController.meuIndex, gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis, gameController.jogadores[gameController.meuIndex].qtdMisseisDisponiveis);
        }
        FecharPanelCartas();
    }

    public void AtualizarInfoCartas(int indexJogadorResponse, int qtdCartasResponse)
    {
        PV.RPC("RPC_AtualizarInfoCartas", RpcTarget.All, indexJogadorResponse, qtdCartasResponse);
    }

    [PunRPC]
    void RPC_AtualizarInfoCartas(int indexJogadorResponse, int qtdCartasResponse)
    {
        gameController.listaPlayersItemInGame[indexJogadorResponse].txInfoQtdCartas.text = qtdCartasResponse + "";
    }

    public void AtualizarInfoArmamentos(int indexJogadorResponse, int qtdTanquesResponse, int qtdMisseisResponse)
    {
        PV.RPC("RPC_AtualizarInfoArmamentos", RpcTarget.All, indexJogadorResponse, qtdTanquesResponse, qtdMisseisResponse);
    }

    [PunRPC]
    void RPC_AtualizarInfoArmamentos(int indexJogadorResponse, int qtdTanquesResponse, int qtdMisseisResponse)
    {
        gameController.listaPlayersItemInGame[indexJogadorResponse].txInfoQtdTanques.text = qtdTanquesResponse + "";
        gameController.listaPlayersItemInGame[indexJogadorResponse].txInfoQtdMisseis.text = qtdMisseisResponse + "";
    }

    public void destruirInstancia(Jogador donoResponse, Carta cartaResponse)
    {
        foreach (Transform child in cartasContent)
        {
            if(child.GetComponent<Carta>().idCarta == cartaResponse.idCarta)
            {
                Destroy(child.gameObject);
            }
        }
        donoResponse.cartas.Remove(cartaResponse);
        textQtdCartas.text = donoResponse.cartas.Count + "";
    }

    public void AtivarPanelBotoesCartas()
    {
        panelBotoesCartas.SetActive(true);
        if (objetivosController.meuObjetivo != null && objetivosController.meuObjetivo.gameObject.activeSelf) objetivosController.meuObjetivo.gameObject.SetActive(false);
    }

    public void DesativarPanelBotoesCartas()
    {
        panelBotoesCartas.SetActive(false);
    }

    [PunRPC]
    void RPC_TrocaCartaAnimacao(int indexJogadorResponse, int[] tipoCarta, int[] valorCarta){
        //if(gameController.meuIndex != indexJogadorResponse){
            for(int i=0; i < tipoCarta.Length; i++){
                trocaCartasAnim.AdicionarCarta(indexJogadorResponse, tipoCarta[i], valorCarta[i]);
            }
        //}
    }


}
