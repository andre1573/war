using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Objetivo : MonoBehaviour
{
    [HideInInspector][SerializeField] public int indexDono;
    [SerializeField] public Continente[] continentesParaConquistar;
    [SerializeField] public TMP_Text textCabecalho, textDescricao, textTitulo;
    [SerializeField] public bool isTemDono;

    private void Awake()
    {
        isTemDono = false;
        textCabecalho.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Conquistar os continentes:" : "Conquer the continents:";
        textDescricao.text = "";
        foreach (Continente continente in continentesParaConquistar)
        {
            string nomeContinente = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? continente.nomePortugues : continente.nomeIngles;
            textDescricao.text += "\n- " + nomeContinente.ToUpper();
        }
        textTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Seu Objetivo" : "Your Objective";
        gameObject.SetActive(false);
    }

}
