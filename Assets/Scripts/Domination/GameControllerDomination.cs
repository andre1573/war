using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Leguar.HiddenVars;
using Photon.Realtime;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using TMPro;
using ExitGames.Client.Photon;
using System.Linq;

[RequireComponent(typeof(PhotonView))]
public class GameControllerDomination : MonoBehaviourPunCallbacks
{

    PhotonView PV;
    [HideInInspector] public HiddenVars hiddenVars;

    [SerializeField] public float tempoPorRound = 60;
    [SerializeField] public PlayerListItemInGame[] listaPlayersItemInGame;
    [SerializeField] public GameObject painelDistribuicao, painelAtaque, painelRemanejo, painelAguardeSuaVez, painelInformacoesExtrasJogadores;
    [SerializeField] public ModalAdicionarExercitos modalAdicionarExercitos;
    [SerializeField] public ControleObjetivos controleObjetivos;
    [SerializeField] public ControleCartas cartasController;
    [SerializeField] public Temporizador temporizadorController;
    [SerializeField] public TMP_Text textMensagem;
    [SerializeField] public RawImage meuBoardImg;
    [SerializeField] public SoundsController soundsController;

    [SerializeField][HideInInspector] public int meuIndex, indexJogadorAtual, indexPrimeiroJogador, qtdPlayerInRoom, turnoAtual;
    [SerializeField][HideInInspector] private float tempoRound;
    [SerializeField][HideInInspector] public bool isJogoComecou, isJogoFinalizado;
    [SerializeField][HideInInspector] public bool isConquistouTerritorioNoTurno;
    [SerializeField][HideInInspector] public TipoFaseTurno tipoFaseTurno; 
    [SerializeField] private Dictionary<int, Player> players;

    [SerializeField] [HideInInspector] public List<Jogador> jogadores;
    [SerializeField] public Continente[] continentes;
    [SerializeField] public List<Territorio> territorios;
    [SerializeField] [HideInInspector] public Territorio territorioAtacando, territorioDefendendo, territorioRemanejoBase, territorioRemanejoDestino;
    [SerializeField] public Dado dadoATK0, dadoATK1, dadoATK2, dadoDEF0, dadoDEF1, dadoDEF2;
    [SerializeField] public Animation novaCartaAnim;

    public enum TipoFaseTurno
    {
        DISTRIBUICAO, ATAQUE, REMANEJO
    }

    public string TipoFaseTurnoDescricaoIndo(TipoFaseTurno tipo)
    {
        if (TipoFaseTurno.DISTRIBUICAO.Equals(tipo)) return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Distribuindo" : "Distributing";
        if (TipoFaseTurno.ATAQUE.Equals(tipo)) return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Atacando" : "Attacking";
        if (TipoFaseTurno.REMANEJO.Equals(tipo)) return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Remanejando" : "Rearranging";
        return "";
    }
    void Awake()
    {
        isJogoComecou = false;
        isJogoFinalizado = false;
        hiddenVars = new HiddenVars();
        PV = GetComponent<PhotonView>();
        meuIndex = -1;
        indexJogadorAtual = -1;
        jogadores = new List<Jogador>();
        obterTodosOsTerritoriosDoGame();
        tempoRound = tempoPorRound;
        temporizadorController.iniciarVariaveis(tempoRound);
        turnoAtual = 0;
        isConquistouTerritorioNoTurno = false;
        cartasController.esvaziarListaCartasSelecionadas();
    }

    private void obterTodosOsTerritoriosDoGame()
    {
        territorios = new List<Territorio>();
        foreach(Continente continente in continentes)
        {
            foreach(Territorio territorio in continente.territorios)
            {
                territorios.Add(territorio);
            }
        }
    }

    void Start()
    {
        ObterPlayersPhoton();
        if (!PhotonNetwork.IsMasterClient) return;
        SortearIndexPrimeiroJogador();
        distribuirTerritoriosIniciaisParaOsJogadores();
        PV.RPC("RPC_IniciarVariaveisInicioGame", RpcTarget.All, indexJogadorAtual);
    }
    
    void Update()
    {
        if (isJogoFinalizado) return;
        if (PhotonNetwork.IsMasterClient)
        {
            if (isJogoComecou)
            {
                if (tempoRound > 0) //Algum Player esta jogando
                {
                    if (!jogadores[indexJogadorAtual].estaConectado || jogadores[indexJogadorAtual].estaEliminado)
                    {
                        tempoRound = tempoPorRound;
                        PV.RPC("RPC_AtualizarVariaveisFimDoRound", RpcTarget.All, indexJogadorAtual, false);
                    }
                    tempoRound -= Time.deltaTime;
                }
                else //fim do round do player
                {
                    PV.RPC("RPC_PassarVezDoJogadorAtual", RpcTarget.All, indexJogadorAtual);
                    tempoRound = tempoPorRound;
                }
            }
        }
    }

    private void FimDoRound()
    {
        modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(false);
        if (verificarObjetivo())
        {
            textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Parabéns, Você completou seu objetivo e venceu o jogo!" : "Congratulations, you've completed your objective and won the game!";
        }
        else
        {
            PV.RPC("RPC_AtualizarVariaveisFimDoRound", RpcTarget.All, indexJogadorAtual, isConquistouTerritorioNoTurno);
        }
    }

    private void mudarJogadorAtual()
    {
        listaPlayersItemInGame[indexJogadorAtual].GetComponent<PlayerListItemInGame>().ToggleBordaJogadorAtual(false);
        indexJogadorAtual++;
        if (indexJogadorAtual > qtdPlayerInRoom-1) indexJogadorAtual = 0;
        listaPlayersItemInGame[indexJogadorAtual].GetComponent<PlayerListItemInGame>().ToggleBordaJogadorAtual(true);
        changeTabuleiroAtivo();
    }

    private void SortearIndexPrimeiroJogador()
    {
        indexJogadorAtual = Random.Range(0, qtdPlayerInRoom); //TODO: setar aleatorio quem joga primeiro
        indexPrimeiroJogador = indexJogadorAtual;
    }

    private void ObterPlayersPhoton()
    {
        while (players == null || players.Count != PhotonNetwork.CurrentRoom.PlayerCount)
        {
            players = PhotonNetwork.CurrentRoom.Players;
        }
        setarListaPlayersAtivos();
    }

    [PunRPC]
    void RPC_AtualizarTurnoAtual(int turnoAtualResponse)
    {
        turnoAtual = turnoAtualResponse;
    }

    [PunRPC]
    void RPC_AtualizarVariaveisFimDoRound(int indexJogadorAtualResponse, bool isConquistouTerritorioResponse)
    {
        indexJogadorAtual = indexJogadorAtualResponse;
        if (isConquistouTerritorioResponse && jogadores[indexJogadorAtualResponse].cartas.Count < 5)
        {
            if(indexJogadorAtualResponse == meuIndex)
            {
                jogadores[indexJogadorAtualResponse].ObterNovaCarta();
            }
            textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "O jogador recebeu uma carta!" : "The player received a letter!";
        }
        else
        {
            textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "O jogador não recebeu carta nesse turno!" : "The player has not received a card this turn!";
        }
        tempoRound = tempoPorRound;
        temporizadorController.ReiniciaTemporizador(tempoRound);
        isConquistouTerritorioNoTurno = false;
        cartasController.esvaziarListaCartasSelecionadas();
        modalAdicionarExercitos.ReiniciarVariaveisPorTurno();
        mudarJogadorAtual();
        if (PhotonNetwork.IsMasterClient && indexJogadorAtual == indexPrimeiroJogador)
        {
            turnoAtual++;
            PV.RPC("RPC_AtualizarTurnoAtual", RpcTarget.All, turnoAtual);
        }
    }

    public Color diminuirBrilhoDaCor(Color cor)
    {
        float tonalidade = 2f;
        return new Color(cor.r / tonalidade, cor.g / tonalidade, cor.b / tonalidade);
    }

    public void selecionarTerritorioAtacando(Territorio territorioResponse)
    {
        if (territorioAtacando != null)
        {
            territorioAtacando.informacoesTerritorio.marcacaoFronteiras.SetActive(false);
            territorioAtacando.imagem.color =territorioAtacando.dono.color;
        }
        territorioAtacando = territorioResponse;
        if (territorioAtacando != null)
        {
            territorioAtacando.imagem.color = diminuirBrilhoDaCor(territorioAtacando.dono.color);
            territorioAtacando.informacoesTerritorio.marcacaoFronteiras.SetActive(true);
        }
    }

    public void selecionarTerritorioRemanejoBase(Territorio territorioResponse)
    {
        if (territorioRemanejoBase != null)
        {
            territorioRemanejoBase.imagem.color = territorioRemanejoBase.dono.color;
            territorioRemanejoBase.informacoesTerritorio.marcacaoFronteiras.SetActive(false);
        }
        territorioRemanejoBase = territorioResponse;
        if (territorioRemanejoBase != null)
        {
            territorioRemanejoBase.informacoesTerritorio.marcacaoFronteiras.SetActive(true);
        }
    }

    [PunRPC]
    void RPC_IniciarVariaveisInicioGame(int indexJogadorAtualResponse)
    {
        isJogoComecou = true;
        indexJogadorAtual = indexJogadorAtualResponse;
        indexPrimeiroJogador = indexJogadorAtual;
        listaPlayersItemInGame[indexJogadorAtual].GetComponent<PlayerListItemInGame>().ToggleBordaJogadorAtual(true);
        changeTabuleiroAtivo();
    }

    private void setarListaPlayersAtivos()
    {
        jogadores = new List<Jogador>();
        qtdPlayerInRoom = players.Count;
        int i = 0;
        List<Player> playersList = new List<Player>();
        playersList.AddRange(players.Values);
        playersList.Sort();
        
        foreach (Player player in playersList)
        {
            Jogador novoJogador = new Jogador();
            novoJogador.userId = player.UserId;
            novoJogador.nickname = player.NickName;
            novoJogador.index = i;
            novoJogador.territorios = new List<Territorio>();
            novoJogador.cartas = new List<Carta>();
            novoJogador.cor = obterCorPorIndex(i);
            novoJogador.color = obterColorPorIndex(i);
            novoJogador.estaConectado = true;
            novoJogador.estaEliminado = false;
            jogadores.Add(novoJogador);

            listaPlayersItemInGame[i].SetUp(novoJogador);
            if (novoJogador.userId == PhotonNetwork.LocalPlayer.UserId)
            {
                meuIndex = i;
                meuBoardImg.color = obterColorPorIndex(meuIndex);
            }
            Debug.Log("novo jogador: " + i + " userId: " + novoJogador.userId);
            i++;
        }
        Debug.Log("jogadores: " + jogadores.Count);
        if (listaPlayersItemInGame.Length > players.Count)
        {
            for (int j = players.Count; j < listaPlayersItemInGame.Length; j++)
            {
                listaPlayersItemInGame[j].gameObject.GetComponent<PlayerListItemInGame>().DesativarInfo();
                listaPlayersItemInGame[j].gameObject.SetActive(false);
            }
        }
    }

    private void changeTabuleiroAtivo()
    {
        tipoFaseTurno = TipoFaseTurno.DISTRIBUICAO;
        AtualizarPanelAguardeSuaVez(indexJogadorAtual, TipoFaseTurno.DISTRIBUICAO);
        if (indexJogadorAtual == meuIndex && jogadores[meuIndex].estaEliminado)
        {
            textMensagem.text = "";
            painelDistribuicao.SetActive(false);
            painelAtaque.SetActive(false);
            painelRemanejo.SetActive(false);
            painelAguardeSuaVez.SetActive(false);
            cartasController.DesativarPanelBotoesCartas();
        }
        else if (indexJogadorAtual == meuIndex && !jogadores[meuIndex].estaEliminado)
        {
            textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Distribua seus exércitos!" : "Deploy your armies!";
            painelDistribuicao.SetActive(true);
            painelAtaque.SetActive(false);
            painelRemanejo.SetActive(false);
            painelAguardeSuaVez.SetActive(false);
            soundsController.TocarSoundYourTurn();
            jogadores[indexJogadorAtual].DistribuirExercitosNoRound();
            modalAdicionarExercitos.SelecionarAbaSoldadosInFaseDistribuicao();
            cartasController.AtivarPanelBotoesCartas();
        }
        else
        {
            textMensagem.text = (PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Vez do jogador " : "Player's turn ") + jogadores[indexJogadorAtual].textNicknameCor();
            painelDistribuicao.SetActive(false);
            painelAtaque.SetActive(false);
            painelRemanejo.SetActive(false);
            painelAguardeSuaVez.SetActive(true);
            cartasController.DesativarPanelBotoesCartas();
        }
    }

    private void distribuirTerritoriosIniciaisParaOsJogadores()
    {
        int qtdTerritorios = territorios.Count;
        int qtdJogadores = jogadores.Count;
        int qtdTerritoriosMaxPorJogador = qtdTerritorios / qtdJogadores;
        int territoriosRestantes = qtdTerritorios;
        int[] indexJogadoresNosTerritorios = new int[qtdTerritorios];

        for (int i = 0; i < qtdJogadores; i++)
        {
            int qtdTerritoriosParaAtribuir = Mathf.Min(qtdTerritoriosMaxPorJogador, territoriosRestantes);

            for (int j = 0; j < qtdTerritoriosParaAtribuir; j++)
            {
                int indexTerritorio = Random.Range(0, qtdTerritorios);
                while (indexJogadoresNosTerritorios[indexTerritorio] != 0)
                {
                    indexTerritorio = Random.Range(0, qtdTerritorios);
                }

                indexJogadoresNosTerritorios[indexTerritorio] = i;
                territoriosRestantes--;
            }
        }

        foreach (Jogador jogador in jogadores)
        {
            jogador.indexObjetivo = controleObjetivos.ObterObjetivoAleatorio(jogador.index);
            PV.RPC("RPC_InformarObjetivoDoJogador", RpcTarget.All, jogador.index, jogador.indexObjetivo);
        }

        PV.RPC("RPC_AtualizarTerritorios", RpcTarget.All, indexJogadoresNosTerritorios);
    }

    private bool possuiJogadorFaltandoCompletarQtdMaxTerritorios(int qtdJogadores, int[] qtdTerritorioPorJogadorIndex, int qtdTerritoriosMaxPorJogador)
    {
        for(int i=0; i< qtdJogadores; i++)
        {
            if(qtdTerritorioPorJogadorIndex[i] < qtdTerritoriosMaxPorJogador)
            {
                return true;
            }
        }
        return false;
    }

    [PunRPC]
    void RPC_InformarObjetivoDoJogador(int indexJogadorResponse, int indexObjetivoResponse)
    {
        jogadores[indexJogadorResponse].indexObjetivo = indexObjetivoResponse;
        if(indexJogadorResponse == meuIndex)
        {
            controleObjetivos.meuObjetivo = controleObjetivos.objetivos[indexObjetivoResponse];
        }
    }

    [PunRPC]
    void RPC_AtualizarTerritorios(int[] indexJogadoresNosTerritorios)
    {
        for (int i = 0; i < territorios.Count; i++)
        {
            territorios[i].setarQtdSoldadosNoTerritorio(1);
            territorios[i].setarQtdTanquesNoTerritorio(0);
            territorios[i].setarDonoTerritorio(jogadores[indexJogadoresNosTerritorios[i]]);
            territorios[i].indexDono = indexJogadoresNosTerritorios[i];
        }
    }

    public void MudarFaseTurno() //Botao Confirmar da tela
    {
        if (modalAdicionarExercitos.territorio != null) { modalAdicionarExercitos.territorio.AtualizarTextQtdExercitos(); modalAdicionarExercitos.territorio.imagem.color = modalAdicionarExercitos.territorio.dono.color; modalAdicionarExercitos.territorio = null; }
        if (territorioAtacando != null) territorioAtacando.imagem.color = territorioAtacando.dono.color; territorioAtacando = null;
        if (territorioDefendendo != null) territorioDefendendo.imagem.color = territorioDefendendo.dono.color; territorioDefendendo = null;
        selecionarTerritorioRemanejoBase(null);
        if (territorioRemanejoDestino != null) territorioRemanejoDestino.imagem.color = territorioRemanejoDestino.dono.color; territorioRemanejoDestino = null;

        if (turnoAtual == 0)
        {
            painelDistribuicao.SetActive(false);
            painelAtaque.SetActive(false);
            painelRemanejo.SetActive(false);
            modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(false);
            modalAdicionarExercitos.SelecionarAbaSoldadosInFaseDistribuicao();
            cartasController.DesativarPanelBotoesCartas();
            FimDoRound();
        }
        else
        {
            if (tipoFaseTurno == TipoFaseTurno.DISTRIBUICAO)
            {
                tipoFaseTurno = TipoFaseTurno.ATAQUE;
                AtualizarPanelAguardeSuaVez(indexJogadorAtual, tipoFaseTurno);
                textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Ataque!" : "Attack!";
                painelDistribuicao.SetActive(false);
                painelAtaque.SetActive(true);
                painelRemanejo.SetActive(false);
                modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(true);
                modalAdicionarExercitos.SelecionarAbaSoldadosInFaseAtaque();
                cartasController.DesativarPanelBotoesCartas();
            }
            else if (tipoFaseTurno == TipoFaseTurno.ATAQUE)
            {
                tipoFaseTurno = TipoFaseTurno.REMANEJO;
                AtualizarPanelAguardeSuaVez(indexJogadorAtual, tipoFaseTurno);
                textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Remaneje!" : "Remanage!";
                painelDistribuicao.SetActive(false);
                painelAtaque.SetActive(false);
                painelRemanejo.SetActive(true);
                modalAdicionarExercitos.qtdVezesRemanejou = 0;
                modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(true);
                modalAdicionarExercitos.SelecionarAbaSoldadosInFaseRemanejo();
                cartasController.DesativarPanelBotoesCartas();
            }
            else if (tipoFaseTurno == TipoFaseTurno.REMANEJO)
            {
                if (modalAdicionarExercitos.territorio != null) { modalAdicionarExercitos.territorio.AtualizarTextQtdExercitos(); modalAdicionarExercitos.territorio.imagem.color = modalAdicionarExercitos.territorio.dono.color; modalAdicionarExercitos.territorio = null; }
                if (territorioAtacando != null) territorioAtacando.imagem.color = territorioAtacando.dono.color;
                if (territorioDefendendo != null) territorioDefendendo.imagem.color = territorioDefendendo.dono.color;
                selecionarTerritorioRemanejoBase(null);
                if (territorioRemanejoDestino != null) territorioRemanejoDestino.imagem.color = territorioRemanejoDestino.dono.color;
                painelDistribuicao.SetActive(false);
                painelAtaque.SetActive(false);
                painelRemanejo.SetActive(false);
                modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(false);
                modalAdicionarExercitos.SelecionarAbaSoldadosInFaseDistribuicao();
                cartasController.DesativarPanelBotoesCartas();
                FimDoRound();
            }
        }
    }

    [PunRPC]
    void RPC_PassarVezDoJogadorAtual(int indexJogadorAtualResponse)
    {
        if(meuIndex == indexJogadorAtualResponse)
        {
            acoesFimRemanejoOuTurno();
        }
    }

    private void acoesFimRemanejoOuTurno()
    {
        Debug.Log("acoesfim: atual: " + indexJogadorAtual + " meuindex: "+meuIndex);
        painelDistribuicao.SetActive(false);
        painelAtaque.SetActive(false);
        painelRemanejo.SetActive(false);
        modalAdicionarExercitos.botaoMudarFaseTurno.SetActive(false);
        modalAdicionarExercitos.SelecionarAbaSoldadosInFaseDistribuicao();
        cartasController.DesativarPanelBotoesCartas();
        FimDoRound();
    }

    public void ResetarTerritorioRemanejoBaseDestino()
    {
        if(indexJogadorAtual == meuIndex && tipoFaseTurno == GameControllerDomination.TipoFaseTurno.REMANEJO)
        {
            selecionarTerritorioRemanejoBase(null);
            if (territorioRemanejoDestino != null) territorioRemanejoDestino.imagem.color = territorioRemanejoBase.dono.color;
            modalAdicionarExercitos.txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Selecione um território" : "Select a territory";
            modalAdicionarExercitos.txTerritorioDestino.text = "";
            territorioRemanejoDestino = null;
            modalAdicionarExercitos.setContadorExercitosRemanejamento(0);
            modalAdicionarExercitos.botoesRemanejo.SetActive(false);
        }
    }

    private string obterCorPorIndex(int index)
    {
        if(index == 0)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Cinza" : "Gray";
        }
        else if (index == 1)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Rosa" : "Pink";
        }
        else if (index == 2)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Vermelho" : "Red";
        }
        else if (index == 3)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Verde" : "Green";
        }
        else if (index == 4)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Azul" : "Blue";
        }
        else if (index == 5)
        {
            return PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Amarelo" : "Yellow";
        }
        else
        {
            return "";
        }
    }

    private Color obterColorPorIndex(int index)
    {
        if (index == 0)
        {
            return Color.gray; //Cinza
        }
        else if (index == 1)
        {
            return new Color(0.5f, 0, 0.5f, 1); //Rosa
        }
        else if (index == 2)
        {
            return new Color(0.5f, 0, 0, 1); //Vermelho
        }
        else if (index == 3)
        {
            return new Color(0, 0.5f, 0, 1); //Verde
        }
        else if (index == 4)
        {
            return new Color(0, 0, 0.5f, 1); //Azul
        }
        else if (index == 5)
        {
            return new Color(0.5f, 0.5f, 0, 1); //Amarelo
        }
        else
        {
            return Color.clear;
        }
    }

    //------------------------- ATAQUE --------------------------------------

    public void AtacarTerritorio()
    {
        int qtdExercitosAtacando = getQtdSoldadosNoTerritorioAtacando();
        if (verificarSePodeAtacar())
        {
            jogarDadosAtaque();
            bool conquistou = false;
            int qtdExercitosSobreviventesPosAtaque = qtdExercitosAtacando - (qtdExercitosAtacando - getQtdSoldadosNoTerritorioAtacando());
            if (territorioDefendendo.qtdSoldadosNoTerritorio <= 0) //Atacou e conquistou territorio
            {
                isConquistouTerritorioNoTurno = true;
                conquistou = true;
                territorioDefendendo.informacoesTerritorio.AtivarAnimacaoConquistado();
                if (qtdExercitosSobreviventesPosAtaque == 2)
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio = 1;
                    territorioAtacando.qtdSoldadosNoTerritorio--;
                }
                else if (qtdExercitosSobreviventesPosAtaque == 3)
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio = 2;
                    territorioAtacando.qtdSoldadosNoTerritorio -= 2 ;
                }
                else if (qtdExercitosSobreviventesPosAtaque > 3)
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio = 3;
                    territorioAtacando.qtdSoldadosNoTerritorio -= 3;
                }
                territorioDefendendo.qtdTanquesNoTerritorio = 0;
            }
            PV.RPC("RPC_AtualizarVariaveisPosAtaque", RpcTarget.All, territorioAtacando.idTerritorio, territorioDefendendo.idTerritorio, territorioAtacando.indexDono, 
                territorioDefendendo.indexDono, territorioAtacando.qtdSoldadosNoTerritorio, territorioDefendendo.qtdSoldadosNoTerritorio, territorioAtacando.qtdTanquesNoTerritorio, territorioDefendendo.qtdTanquesNoTerritorio, conquistou);
        }
        
    }

    public void AtacarTerritorioComMissil()
    {
        if (verificarSePodeAtacar())
        {
            jogarDadosAtaque();
           
            PV.RPC("RPC_AtualizarVariaveisPosAtaque", RpcTarget.All, territorioAtacando.idTerritorio, territorioDefendendo.idTerritorio, territorioAtacando.indexDono,
                territorioDefendendo.indexDono, territorioAtacando.qtdSoldadosNoTerritorio, territorioDefendendo.qtdSoldadosNoTerritorio, territorioAtacando.qtdTanquesNoTerritorio, territorioDefendendo.qtdTanquesNoTerritorio, false);
        }

    }

    private bool verificarObjetivo()
    {
        return this.controleObjetivos.verificarObjetivoFinalizado();
    }

    private bool verificarSePodeAtacar()
    {
        if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Soldados)
        {
            if(territorioAtacando.qtdSoldadosNoTerritorio > 1)
            {
                return true;
            }
            else
            {
                textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "É necessário ter pelo menos 2 exércitos no território!" : "It is necessary to have at least 2 armies in the territory!";
            }
        }
        else if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Tanques)
        {
            if (territorioAtacando.qtdTanquesNoTerritorio >= 1)
            {
                if(territorioDefendendo.qtdSoldadosNoTerritorio > 1)
                {
                    return true;
                }
                else
                {
                    textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Só é possível atacar com tanque se o território de defesa possuir 2 ou mais soldados!" : "It is only possible to attack with a tank if the defending territory has 2 or more soldiers!";
                }
            }
            else
            {
                textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "É necessário ter pelo menos 1 tanque no território!" : "It is necessary to have at least 1 tank in the territory!";
            }
        }
        else if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Misseis)
        {
            if(jogadores[meuIndex].qtdMisseisDisponiveis > 0)
            {
                return true;
            }
            else
            {
                textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "É necessário ter pelo menos 1 míssil disponível para efetuar o ataque!" : "You must have at least 1 missile available to carry out the attack!";
            }
        }
        return false;
    }

    private int getQtdSoldadosNoTerritorioAtacando()
    {
        if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Soldados)
        {
            return territorioAtacando.qtdSoldadosNoTerritorio;
        }
        else if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Tanques)
        {
            return territorioAtacando.qtdTanquesNoTerritorio;
        }
        else
        {
            return 0;
        }
    }

    private void jogarDadosAtaque()
    {
        if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Soldados)
        {
            jogarDadosAtaqueSoldados();
        }
        else if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Tanques)
        {
            jogarDadosAtaqueTanques();
        }
        else if (modalAdicionarExercitos.tipoAbaAtaque == ModalAdicionarExercitos.TipoAbaArmamentos.Misseis)
        {
            jogarDadosAtaqueMisseis();
        }
    }

    private void jogarDadosAtaqueSoldados()
    {
        List<int> dadosAtaque = new List<int> { 0, 0, 0 };
        List<int> dadosDefesa = new List<int> { 0, 0, 0 };

        bool isVenceuContraTanque = false;

        if (territorioDefendendo.qtdTanquesNoTerritorio >= 1) //Defendendo com tanque
        {
            dadosAtaque[0] = 0;
            dadosAtaque[1] = Random.Range(1, 7);
            dadosAtaque[2] = Random.Range(1, 7);
            dadosAtaque.Sort();

            dadosDefesa[0] = Random.Range(1, 7);
            dadosDefesa[1] = Random.Range(1, 7);
            dadosDefesa[2] = Random.Range(1, 7);
            dadosDefesa.Sort();

            if (dadosAtaque[2] > dadosDefesa[2])
            {
                territorioDefendendo.qtdTanquesNoTerritorio--;
                isVenceuContraTanque = true;
            }
            else
            {
                territorioAtacando.qtdSoldadosNoTerritorio -= dadosDefesa[2];
            }
            if (territorioAtacando.qtdSoldadosNoTerritorio <= 0) territorioAtacando.qtdSoldadosNoTerritorio = 1;
        }
        else
        {
            if (territorioAtacando.qtdSoldadosNoTerritorio == 2)
            {
                dadosAtaque[0] = 0;
                dadosAtaque[1] = 0;
                dadosAtaque[2] = Random.Range(1, 7);
            }
            else if (territorioAtacando.qtdSoldadosNoTerritorio == 3)
            {
                dadosAtaque[0] = 0;
                dadosAtaque[1] = Random.Range(1, 7);
                dadosAtaque[2] = Random.Range(1, 7);
                dadosAtaque.Sort();
            }
            else if (territorioAtacando.qtdSoldadosNoTerritorio >= 4)
            {
                dadosAtaque[0] = Random.Range(1, 7);
                dadosAtaque[1] = Random.Range(1, 7);
                dadosAtaque[2] = Random.Range(1, 7);
                dadosAtaque.Sort();
            }

            if (territorioDefendendo.qtdSoldadosNoTerritorio == 1)
            {
                dadosDefesa[0] = 0;
                dadosDefesa[1] = 0;
                dadosDefesa[2] = Random.Range(1, 7);
            }
            else if (territorioDefendendo.qtdSoldadosNoTerritorio == 2)
            {
                dadosDefesa[0] = 0;
                dadosDefesa[1] = Random.Range(1, 7);
                dadosDefesa[2] = Random.Range(1, 7);
                dadosDefesa.Sort();
            }
            else if (territorioDefendendo.qtdSoldadosNoTerritorio >= 3)
            {
                dadosDefesa[0] = Random.Range(1, 7);
                dadosDefesa[1] = Random.Range(1, 7);
                dadosDefesa[2] = Random.Range(1, 7);
                dadosDefesa.Sort();
            }

            if (dadosAtaque[2] != 0 && dadosDefesa[2] != 0)
            {
                if (dadosAtaque[2] > dadosDefesa[2])
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio--;
                }
                else
                {
                    territorioAtacando.qtdSoldadosNoTerritorio--;
                }
            }

            if (dadosAtaque[1] != 0 && dadosDefesa[1] != 0)
            {
                if (dadosAtaque[1] > dadosDefesa[1])
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio--;
                }
                else
                {
                    territorioAtacando.qtdSoldadosNoTerritorio--;
                }
            }

            if (dadosAtaque[0] != 0 && dadosDefesa[0] != 0)
            {
                if (dadosAtaque[0] > dadosDefesa[0])
                {
                    territorioDefendendo.qtdSoldadosNoTerritorio--;
                }
                else
                {
                    territorioAtacando.qtdSoldadosNoTerritorio--;
                }
            }
        }
        PV.RPC("RPC_DirecionarImagemTiroParaAlvo", RpcTarget.All, territorioAtacando.idTerritorio, territorioDefendendo.idTerritorio);
        if (territorioAtacando != null) territorioAtacando.informacoesTerritorio.AtivarAnimacaoAtacando(modalAdicionarExercitos.tipoAbaAtaque);
        if (territorioDefendendo != null) territorioDefendendo.informacoesTerritorio.AtivarAnimacaoDefendendoAtaque();
        if (isVenceuContraTanque)
        {
            territorioDefendendo.informacoesTerritorio.AtivarAnimacaoTanqueFalhou();
        }
        PV.RPC("RPC_AtualizarDadosAtkDef", RpcTarget.All, dadosAtaque[0], dadosAtaque[1], dadosAtaque[2], dadosDefesa[0] , dadosDefesa[1], dadosDefesa[2]);
    }

    [PunRPC]
    void RPC_DirecionarImagemTiroParaAlvo(int idTerritorioAtacando, int idTerritorioDefendendo)
    {
        Territorio terrAtk = territorios.Find(x => x.idTerritorio == idTerritorioAtacando);
        Territorio terrDef = territorios.Find(x => x.idTerritorio == idTerritorioDefendendo);
        if (terrDef != null && terrAtk != null)
        {
            terrAtk.informacoesTerritorio.direcionarImagemParaAlvoController.objAlvo = terrDef.informacoesTerritorio.imgBaseTiroSoldados.transform;
        }
    }

    private void jogarDadosAtaqueTanques()
    {
        List<int> dadosAtaque = new List<int> { 0, 0, 0 };
        List<int> dadosDefesa = new List<int> { 0, 0, 0 };
        bool ataqueVenceu = false;
        if (territorioAtacando.qtdTanquesNoTerritorio >= 1)
        {
            dadosAtaque[0] = Random.Range(1, 7);
            dadosAtaque[1] = Random.Range(1, 7);
            dadosAtaque[2] = Random.Range(1, 7);
            dadosAtaque.Sort();
        }

        if (territorioDefendendo.qtdTanquesNoTerritorio >= 1) //Defendendo com tanque
        {
            dadosDefesa[0] = Random.Range(1, 7);
            dadosDefesa[1] = Random.Range(1, 7);
            dadosDefesa[2] = Random.Range(1, 7);
            dadosDefesa.Sort();

            if (dadosAtaque[2] > dadosDefesa[2])
            {
                territorioDefendendo.qtdTanquesNoTerritorio--;
                ataqueVenceu = true;
            }
            else
            {
                territorioAtacando.qtdTanquesNoTerritorio--;
                ataqueVenceu = false;
            }
        }
        else
        {
            dadosDefesa[0] = 0;
            dadosDefesa[1] = Random.Range(1, 7);
            dadosDefesa[2] = Random.Range(1, 7);
            dadosDefesa.Sort();

            if (dadosAtaque[2] != 0 && dadosDefesa[2] != 0)
            {
                if (dadosAtaque[2] > dadosDefesa[2])
                {
                    ataqueVenceu = true;
                    territorioDefendendo.qtdSoldadosNoTerritorio -= dadosAtaque[2];
                }
                else
                {
                    ataqueVenceu = false;
                    territorioAtacando.qtdTanquesNoTerritorio--;
                }
            }
            if (territorioDefendendo.qtdSoldadosNoTerritorio <= 0) territorioDefendendo.qtdSoldadosNoTerritorio = 1;
        }
        PV.RPC("RPC_DirecionarImagemTiroParaAlvo", RpcTarget.All, territorioAtacando.idTerritorio, territorioDefendendo.idTerritorio);
        if (ataqueVenceu)
        {
            if (territorioAtacando != null) territorioAtacando.informacoesTerritorio.AtivarAnimacaoAtacando(modalAdicionarExercitos.tipoAbaAtaque);
            if (territorioDefendendo != null) territorioDefendendo.informacoesTerritorio.AtivarAnimacaoDefendendoAtaque();
        }
        else
        {
            if (territorioAtacando != null) territorioAtacando.informacoesTerritorio.AtivarAnimacaoTanqueFalhou();
        }
        PV.RPC("RPC_AtualizarDadosAtkDef", RpcTarget.All, dadosAtaque[0], dadosAtaque[1], dadosAtaque[2], dadosDefesa[0], dadosDefesa[1], dadosDefesa[2]);
    }

    private void jogarDadosAtaqueMisseis()
    {
        List<int> dadosAtaque = new List<int> { 0, 0, 0 };
        List<int> dadosDefesa = new List<int> { 0, 0, 0 };
        bool ataqueVenceu = false;
        if (jogadores[meuIndex].qtdMisseisDisponiveis >= 1)
        {
            dadosAtaque[0] = Random.Range(1, 7);
            dadosAtaque[1] = Random.Range(1, 7);
            dadosAtaque[2] = Random.Range(1, 7);
            dadosAtaque.Sort();

            dadosDefesa[0] = 0;
            dadosDefesa[1] = Random.Range(1, 7);
            dadosDefesa[2] = Random.Range(1, 7);
            dadosDefesa.Sort();

            if (dadosAtaque[2] != 0 && dadosDefesa[2] != 0)
            {
                if (dadosAtaque[2] > dadosDefesa[2])
                {
                    ataqueVenceu = true;
                    territorioDefendendo.qtdSoldadosNoTerritorio = 1;
                    territorioDefendendo.qtdTanquesNoTerritorio = 0;
                    textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "O míssil devastou o território!" : "The missile devastated the territory!";
                }
                else
                {
                    ataqueVenceu = false;
                    textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "O míssil falhou!" : "The missile failed!";
                }
                jogadores[meuIndex].qtdMisseisDisponiveis--;
            }
        }
        if(territorioDefendendo != null) territorioDefendendo.informacoesTerritorio.AtivarAnimacaoRecebendoMissil(ataqueVenceu);
        AtualizarQtdMisseisPosAtaque();
        PV.RPC("RPC_AtualizarDadosAtkDef", RpcTarget.All, dadosAtaque[0], dadosAtaque[1], dadosAtaque[2], dadosDefesa[0], dadosDefesa[1], dadosDefesa[2]);
    }

    private void AtualizarQtdMisseisPosAtaque()
    {
        modalAdicionarExercitos.txMisseisEstoque.text = jogadores[meuIndex].qtdMisseisDisponiveis + "";
        cartasController.AtualizarInfoArmamentos(meuIndex, jogadores[meuIndex].qtdTanquesDisponiveis, jogadores[meuIndex].qtdMisseisDisponiveis);
    }

    [PunRPC]
    void RPC_AtualizarDadosAtkDef(int valorDadoATK0, int valorDadoATK1, int valorDadoATK2, int valorDadoDEF0, int valorDadoDEF1, int valorDadoDEF2)
    {
        dadoATK0.AtualizarValorDado(valorDadoATK0);
        dadoATK1.AtualizarValorDado(valorDadoATK1);
        dadoATK2.AtualizarValorDado(valorDadoATK2);

        dadoDEF0.AtualizarValorDado(valorDadoDEF0);
        dadoDEF1.AtualizarValorDado(valorDadoDEF1);
        dadoDEF2.AtualizarValorDado(valorDadoDEF2);
    }

    [PunRPC]
    void RPC_AtualizarVariaveisPosAtaque(int idTerritorioAtaque, int idTerritorioDefesa, int indexDonoAtaque, int indexDonoDefesa, int qtdSoldadosNoAtaque, int qtdSoldadosNaDefesa, int qtdTanquesNoAtaque, int qtdTanquesNaDefesa, bool conquistou)
    {
        foreach(Territorio territorio in territorios)
        {
            if(idTerritorioAtaque == territorio.idTerritorio)
            {
                territorio.qtdSoldadosNoTerritorio = qtdSoldadosNoAtaque;
                territorio.qtdTanquesNoTerritorio = qtdTanquesNoAtaque;
                territorio.informacoesTerritorio.textQtdExercitos.text = territorio.qtdSoldadosNoTerritorio + "";
                territorio.informacoesTerritorio.textQtdTanques.text = territorio.qtdTanquesNoTerritorio + "";
                if (territorio.qtdTanquesNoTerritorio > 0)
                {
                    territorio.informacoesTerritorio.objTanque.SetActive(true);
                }
                else
                {
                    territorio.informacoesTerritorio.objTanque.SetActive(false);
                }
            }
            else if (idTerritorioDefesa == territorio.idTerritorio)
            {
                if (conquistou)
                {
                    territorio.trocarDonoTerritorio(jogadores[indexDonoDefesa], jogadores[indexDonoAtaque]);
                    verificarSeJogadorPossuiAlgumTerritorioOuFoiEliminado(indexDonoDefesa);
                }
                territorio.qtdSoldadosNoTerritorio = qtdSoldadosNaDefesa;
                territorio.qtdTanquesNoTerritorio = qtdTanquesNaDefesa;
                territorio.informacoesTerritorio.textQtdExercitos.text = territorio.qtdSoldadosNoTerritorio + "";
                territorio.informacoesTerritorio.textQtdTanques.text = territorio.qtdTanquesNoTerritorio + "";
                if (territorio.qtdTanquesNoTerritorio > 0)
                {
                    territorio.informacoesTerritorio.objTanque.SetActive(true);
                }
                else
                {
                    territorio.informacoesTerritorio.objTanque.SetActive(false);
                }
            }
        }
        if(meuIndex == indexDonoAtaque)
        {
            territorioDefendendo = null;
            this.verificarObjetivo();
        }
    }

    private void verificarSeJogadorPossuiAlgumTerritorioOuFoiEliminado(int indexJogadorResponse)
    {
        bool jogadorPossuiAlgumTerritorio = false;
        foreach (Territorio territorio in territorios)
        {
            if(territorio.indexDono == indexJogadorResponse)
            {
                jogadorPossuiAlgumTerritorio = true;
                break;
            }
        }
        if (!jogadorPossuiAlgumTerritorio)
        {
            jogadores[indexJogadorResponse].eliminarJogador();
        }
    }

    public void AtualizarPanelAguardeSuaVez(int indexJogadorAtual, GameControllerDomination.TipoFaseTurno tipoFaseTurnoResponse)
    {
        PV.RPC("RPC_AtualizarPanelAguardeSuaVez", RpcTarget.All, indexJogadorAtual, tipoFaseTurnoResponse);
    }

    [PunRPC]
    void RPC_AtualizarPanelAguardeSuaVez(int indexJogadorAtual, GameControllerDomination.TipoFaseTurno tipoFaseTurnoResponse)
    {
        if(indexJogadorAtual == meuIndex)
        {
            modalAdicionarExercitos.botoesAguardeSuaVez.SetActive(false);
        }
        else
        {
            modalAdicionarExercitos.botoesAguardeSuaVez.SetActive(true);
            if (GameControllerDomination.TipoFaseTurno.DISTRIBUICAO.Equals(tipoFaseTurnoResponse)) modalAdicionarExercitos.ObjImageDistribuindo.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            else modalAdicionarExercitos.ObjImageDistribuindo.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            if (GameControllerDomination.TipoFaseTurno.ATAQUE.Equals(tipoFaseTurnoResponse)) modalAdicionarExercitos.ObjImageAtacando.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            else modalAdicionarExercitos.ObjImageAtacando.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            if (GameControllerDomination.TipoFaseTurno.REMANEJO.Equals(tipoFaseTurnoResponse)) modalAdicionarExercitos.ObjImageRemanejando.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            else modalAdicionarExercitos.ObjImageRemanejando.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            modalAdicionarExercitos.txDescricaoAguardandoJogador.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? jogadores[indexJogadorAtual].nickname + " (" + jogadores[indexJogadorAtual].cor +
                ") está " + TipoFaseTurnoDescricaoIndo(tipoFaseTurnoResponse) + "!" : jogadores[indexJogadorAtual].nickname + " (" + jogadores[indexJogadorAtual].cor + ") is " + TipoFaseTurnoDescricaoIndo(tipoFaseTurnoResponse) + "!";
        }
    }

    public void AbrirInformacoesExtrasJogadores()
    {
        painelInformacoesExtrasJogadores.SetActive(true);
    }

    public void FecharInformacoesExtrasJogadores()
    {
        painelInformacoesExtrasJogadores.SetActive(false);
    }

    public override void OnPlayerLeftRoom(Player playerLeft)
    {
       Debug.Log("Player Disconnected left room: " + playerLeft.NickName);
       foreach(Jogador jogador in jogadores)
       {
            if(jogador.userId == playerLeft.UserId)
            {
                jogador.estaConectado = false;
                break;
            }
       }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("O player " + newPlayer.NickName + " entrou na sala");
        foreach (Jogador jogador in jogadores)
        {
            if (jogador.userId == newPlayer.UserId)
            {
                jogador.estaConectado = true;
                break;
            }
        }
    }

}


