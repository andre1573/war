using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class ControleObjetivos : MonoBehaviourPunCallbacks
{

    [SerializeField] public Objetivo[] objetivos;
    [SerializeField][HideInInspector] public Objetivo meuObjetivo;
    [SerializeField] public GameControllerDomination gameController;
    [SerializeField] public ControleCartas cartasController;
    [SerializeField] public MensagemAlerta mensagemAlerta;
    [SerializeField] public TMP_Text txNomeJogadorVencedor, txObjetivoJogadorVencedor, txMsgRecompensa;
    [SerializeField] public GameObject panelFimDeJogo, panelBaseBoardBotoes;
    [SerializeField] public GameObject buttonFecharCardObjetivo;
    [SerializeField] PlayFabLeaderboard playFabLeaderboard;

    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        foreach(Objetivo obj in objetivos)
        {
            obj.gameObject.SetActive(true);
        }
    }

    public int ObterObjetivoAleatorio(int indexDonoResponse)
    {
        int indexObj = -1;
        while(indexObj == -1 || objetivos[indexObj].isTemDono)
        {
            indexObj = Random.Range(0, objetivos.Length);
        }
        objetivos[indexObj].isTemDono = true;
        objetivos[indexObj].indexDono = indexDonoResponse;
        return indexObj;
    }

    public void AbrirCardObjetivo()
    {
        if (meuObjetivo == null) return;
        if (meuObjetivo.gameObject.activeSelf)
        {
            this.FecharCardObjetivo();
        }
        else
        {
            if (cartasController.panelBotoesCartas.activeSelf) cartasController.FecharPanelCartas();
            meuObjetivo.gameObject.SetActive(true);
            buttonFecharCardObjetivo.SetActive(true);
        }
    }

    public void FecharCardObjetivo()
    {
        if (meuObjetivo == null) return;
        meuObjetivo.gameObject.SetActive(false);
        buttonFecharCardObjetivo.SetActive(false);
    }

    public bool verificarObjetivoFinalizado()
    {
        List<Territorio> territoriosDoObjetivo = new List<Territorio>();
        foreach (Continente continente in meuObjetivo.continentesParaConquistar)
        {
            territoriosDoObjetivo.AddRange(continente.territorios.GetRange(0, continente.territorios.Count));
        }
        int meuIndex = gameController.meuIndex;

        foreach(Territorio territorio in territoriosDoObjetivo)
        {
            if(territorio.indexDono != meuIndex)
            {
                return false;
            }
        }
        PV.RPC("RPC_AtualizarVariaveisVencedor", RpcTarget.All, meuIndex);
        return true;
    }

    [PunRPC]
    void RPC_AtualizarVariaveisVencedor(int indexVencedor)
    {
        txNomeJogadorVencedor.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Vencedor: " : "Winner: ";
        txNomeJogadorVencedor.text += gameController.jogadores[indexVencedor].nickname + " ("+ gameController.jogadores[indexVencedor].cor + ")";
        txNomeJogadorVencedor.color = gameController.jogadores[indexVencedor].color;
        txObjetivoJogadorVencedor.text = objetivos[gameController.jogadores[indexVencedor].indexObjetivo].textDescricao.text;
        panelFimDeJogo.SetActive(true);
        panelBaseBoardBotoes.SetActive(false);
        gameController.isJogoFinalizado = true;
        if(indexVencedor == gameController.meuIndex) //Apenas o vencedor
        {
            int qtdTrofeusRecebidos = 10 * gameController.jogadores.Count + gameController.jogadores[indexVencedor].territorios.Count;
            playFabLeaderboard.SendLeaderBoardStarScore(1);
            playFabLeaderboard.SendLeaderBoardTrofeuScore(qtdTrofeusRecebidos);
            txMsgRecompensa.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Recompensa: +" : "Reward: +";
            txMsgRecompensa.text += qtdTrofeusRecebidos;
            txMsgRecompensa.text += PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? " Trof�us: " : " Trophies: ";
            gameController.soundsController.TocarSoundVictory();
        }
        else //perdedores
        {
            gameController.soundsController.TocarSoundGameover();
            if (!gameController.jogadores[gameController.meuIndex].estaEliminado)
            {
                int qtdTrofeusRecebidos = 10 * gameController.jogadores.Count / 2;
                playFabLeaderboard.SendLeaderBoardTrofeuScore(10);
                txMsgRecompensa.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Recompensa: +" : "Reward: +";
                txMsgRecompensa.text += qtdTrofeusRecebidos;
                txMsgRecompensa.text += PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? " Trof�us" : " Trophies";
            }
            else
            {
                txMsgRecompensa.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Desculpe, voc� n�o recebeu recompensa porque foi eliminado :(" : "Sorry you didn't get reward because you got eliminated :(";
            }
        }
    }

}
