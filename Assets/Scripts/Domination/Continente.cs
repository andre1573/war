using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Continente : MonoBehaviour
{

    public string nomePortugues, nomeIngles;
    [SerializeField] public int bonusSoldados;
    [SerializeField] public List<Territorio> territorios;

}
