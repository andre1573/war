using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PhotonView))]
public class Chat : MonoBehaviourPunCallbacks
{

    [HideInInspector] PhotonView PV;
    [SerializeField] TMP_InputField inputMsg;
    [SerializeField] TMP_Text outputMsgs, notificacoesTxt;
    private bool isAtualizado;
    private int msgsNLidas = 0;
    public GameObject notificacoes, abaChat;


    void Start()
    {
        PV = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter)) && inputMsg.text != "")
        {
            EnviarMensagem();
        }
        if(abaChat.activeSelf && !isAtualizado){
            isAtualizado = true;
            msgsNLidas = 0;
        }

    }

    public void EnviarMensagem()
    {
        PV.RPC("RPC_RecebendoMensagem", RpcTarget.All, inputMsg.text, PhotonNetwork.NickName);
        inputMsg.text = "";
    }

    [PunRPC]
    void RPC_RecebendoMensagem(string mensagemResponse, string usuario)
    {
        outputMsgs.text += "\n" + usuario + ": " + mensagemResponse;
        if(!abaChat.activeSelf){
            isAtualizado = false;
            msgsNLidas++;
            notificacoesTxt.text = msgsNLidas.ToString();
            notificacoes.SetActive(true);
        }
    }

    public void LimparOutputMsgs()
    {
        outputMsgs.text = "";
    }

    public void MsgInicial(string msg)
    {
        outputMsgs.text = msg;
    }



}
