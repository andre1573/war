using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temporizador : MonoBehaviour
{

    [SerializeField] BarraHorizontal barraHorizontal;

    public float tempoAtual;

    public void iniciarVariaveis(float tempoRound)
    {
        tempoAtual = tempoRound;
        barraHorizontal.DefinirValorMaximo(tempoRound);
    }

    private void Update()
    {
        tempoAtual -= Time.deltaTime;
        barraHorizontal.AtualizarBarra(tempoAtual);
    }

    public void ReiniciaTemporizador(float tempoRound)
    {
        barraHorizontal.AtualizarBarra(tempoRound);
        tempoAtual = tempoRound;
    }
   
}
