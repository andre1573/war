using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirecionarImagemParaAlvo : MonoBehaviour
{

    [SerializeField] public Transform objPivot, objAlvo;

    private void Update()
    {
        if(objAlvo != null)
        {
            Vector3 direction = (objAlvo.position - objPivot.transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            objPivot.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

}
