using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Jogador : MonoBehaviour
{

    [SerializeField] public string userId, nickname, cor;
    [SerializeField] public Color color;
    [SerializeField] public int index, indexObjetivo;
    [SerializeField] public List<Territorio> territorios;
    [SerializeField] public List<Carta> cartas;
    [SerializeField] public int qtdSoldadosDisponiveisNoRound, qtdTanquesDisponiveis = 0, qtdMisseisDisponiveis = 0;
    [SerializeField] ControleCartas cartasController;
    [SerializeField] GameControllerDomination gameControllerDomination;
    [SerializeField] MensagemAlerta alerta;
    [SerializeField] PlayFabLeaderboard playFabLeaderboard;
    [SerializeField] public bool estaConectado, estaEliminado;

   


    private void Awake()
    {
        estaConectado = true;
        territorios = new List<Territorio>();
        cartas = new List<Carta>();
        ObterControllers();
    }

    private void Start()
    {
        ObterControllers();
    }

    public void DistribuirExercitosNoRound() //no inicio do turno, o jogador recebe q a quantidade de territorios dividio por 2 em exercitos; minimo 3 exercitos;
    {
        qtdSoldadosDisponiveisNoRound = (territorios.Count %2 != 0 ? territorios.Count+1 : territorios.Count) / 2;
        if (qtdSoldadosDisponiveisNoRound < 3) qtdSoldadosDisponiveisNoRound = 3;
        darBonusPorContinenteConquistado();
    }

    private void darBonusPorContinenteConquistado()
    {
        ObterControllers();
        alerta.txMsgAlertaTitle.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "É a sua vez!" : "It's your turn!";
        bool recebeuAlgumBonus = false;
        string descricaoAlerta = "";
        foreach (Continente continente in gameControllerDomination.continentes)
        {
            bool possuiTodosOsTerritoriosDoContinente = true;
            foreach(Territorio territorio in continente.territorios)
            {
                if(territorio.indexDono != index)
                {
                    possuiTodosOsTerritoriosDoContinente = false;
                    break;
                }
            }
            if (possuiTodosOsTerritoriosDoContinente)
            {
                recebeuAlgumBonus = true;
                qtdSoldadosDisponiveisNoRound += continente.bonusSoldados;
                if(PlayerPrefs.GetInt("INDEXIDIOMA") == 1)
                {
                    descricaoAlerta += "- "+continente.nomePortugues+": "+ "+"+ continente.bonusSoldados + "\n";
                }
                else
                {
                    descricaoAlerta += "- " + continente.nomeIngles + ": " + "+" + continente.bonusSoldados + "\n";
                }
            }
        }
        if (recebeuAlgumBonus)
        {
            alerta.txMsgAlertaSubtitle.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Bônus:" : "Bonus:";
            alerta.txMsgAlertaDescricao.text = descricaoAlerta;
        }
        else
        {
            alerta.txMsgAlertaDescricao.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Você não recebeu bônus de exércitos porque não possui nenhum continente conquistado" : "You didn't receive bonus from armies because you don't have any conquered continents";
        }
        alerta.AtivarMensagemAlerta();
    }

    public void AcrescentarSoldados(int qtdSoldadosResponse)
    {
        qtdSoldadosDisponiveisNoRound += qtdSoldadosResponse;
    }

    public void AcrescentarTanques(int qtdTanquesResponse)
    {
        qtdTanquesDisponiveis += qtdTanquesResponse;
    }

    public void AcrescentarMisseis(int qtdMisseisResponse)
    {
        qtdMisseisDisponiveis += qtdMisseisResponse;
    }

    public void DescrescerTanques(int qtdTanquesResponse)
    {
        qtdTanquesDisponiveis -= qtdTanquesResponse;
    }

    public void ObterNovaCarta()
    {
        ObterControllers();
        Carta cartaNova = Instantiate(cartasController.cartaPrefab, cartasController.cartasContent).GetComponent<Carta>();
        cartaNova.criarCartaAleatoria(cartasController, this);
        Carta cartaAnim = gameControllerDomination.novaCartaAnim.GetComponent<Carta>();
        cartaAnim.AtualizarCarta(cartaNova.tipoCarta, cartaNova.valor);
        cartaAnim.gameObject.SetActive(true);
        gameControllerDomination.novaCartaAnim.Play();
        cartas.Add(cartaNova);
        cartasController.textQtdCartas.text = cartas.Count + "";
        cartasController.AtualizarInfoCartas(index, cartas.Count);
    }

    private void ObterControllers()
    {
        if (cartasController != null && gameControllerDomination != null && alerta != null) return;
        GameObject objController = GameObject.FindGameObjectWithTag("GameController");
        if (cartasController == null) cartasController = objController.GetComponent<ControleCartas>();
        if (gameControllerDomination == null) gameControllerDomination = objController.GetComponent<GameControllerDomination>();
        if (alerta == null) alerta = objController.GetComponent<MensagemAlerta>();
    }

    public string textNicknameCor()
    {
        return this.nickname + " (" + this.cor + ")";
    }

    public void eliminarJogador()
    {
        estaEliminado = true;
    }

}
