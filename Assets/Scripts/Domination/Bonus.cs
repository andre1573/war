using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bonus : MonoBehaviour
{

    public TMP_Text txNomeContinente, txQtdBonus;
    public Continente continente;

    // Start is called before the first frame update
    void Start()
    {
        txNomeContinente.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? continente.nomePortugues : continente.nomeIngles;
        txQtdBonus.text = "+" + continente.bonusSoldados;
        txQtdBonus.text += PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? " Soldados" : " Soldiers";
    }
   
}
