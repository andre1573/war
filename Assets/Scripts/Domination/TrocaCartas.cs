using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrocaCartas : MonoBehaviour
{
    public Animation anim;
    private List<ValoresCarta> cartas;
    [SerializeField]
    public Carta cartaAtual;
    public GameControllerDomination gameController;

    public struct ValoresCarta{
        public int dono{get;set;}
        public int tipo{get;set;}
        public int valor{get;set;}
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animation>();
        cartas = new List<ValoresCarta>();
    }

    public void AdicionarCarta(int dono, int tipo, int valor){
        Debug.Log("Recebido troca carta- id:" + dono + " tipo:" + tipo + " valor:" + valor);
        ValoresCarta carta = new ValoresCarta();
        carta.dono = dono;
        carta.tipo = tipo;
        carta.valor = valor;
        Debug.Log("trocou carta" + carta.tipo + " " + carta.valor);
        cartas.Add(carta);
        if(cartas.Count == 1 && !anim.isPlaying){
            Play();
        }
    }

    public void Play(){
        if(cartas.Count >0){
            cartaAtual.dono = gameController.jogadores[cartas[0].dono];
            if (cartas[0].tipo == (int)Carta.TipoCarta.MULTIPLICADOR)
            {
                cartaAtual.tipoCarta = Carta.TipoCarta.MULTIPLICADOR;
            }
            else if (cartas[0].tipo == (int)Carta.TipoCarta.SOLDADO)
            {
                cartaAtual.tipoCarta = Carta.TipoCarta.SOLDADO;
            }
            else if (cartas[0].tipo == (int)Carta.TipoCarta.TANQUE)
            {
                cartaAtual.tipoCarta = Carta.TipoCarta.TANQUE;
            }
            else if (cartas[0].tipo == (int)Carta.TipoCarta.MISSIL)
            {
                cartaAtual.tipoCarta = Carta.TipoCarta.MISSIL;
            }
            cartaAtual.imgMultiplicador.SetActive(Carta.TipoCarta.MULTIPLICADOR.Equals(cartaAtual.tipoCarta));
            cartaAtual.imgSoldado.SetActive(Carta.TipoCarta.SOLDADO.Equals(cartaAtual.tipoCarta));
            cartaAtual.imgTanque.SetActive(Carta.TipoCarta.TANQUE.Equals(cartaAtual.tipoCarta));
            cartaAtual.imgMissil.SetActive(Carta.TipoCarta.MISSIL.Equals(cartaAtual.tipoCarta));
            cartaAtual.valor = cartas[0].valor;
            cartas.Remove(cartas[0]);
            Debug.Log("Animação ativou. id:" + cartaAtual.dono + " tipo:" + (int)cartaAtual.tipoCarta + " valor:" + cartaAtual.valor);
            Debug.Log(cartas.Count);
            cartaAtual.AtualizarCarta(cartaAtual.tipoCarta, cartaAtual.valor);
            anim.Rewind();
            anim.Play();
            gameController.soundsController.TocarSoundCartasTroca();
        }
        else Debug.Log("Chamando animação de carta sem cartas");
    }
}
