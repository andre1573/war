using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MensagemAlerta : MonoBehaviour
{

    [SerializeField] GameObject objMensagemAlerta;
    [SerializeField] public TMP_Text txMsgAlertaTitle, txMsgAlertaSubtitle, txMsgAlertaDescricao;

    public void AtivarMensagemAlerta()
    {
        objMensagemAlerta.SetActive(true);
    }

    public void DesativarMensagemAlerta()
    {
        objMensagemAlerta.SetActive(false);
    }

}
