using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Carta : MonoBehaviour
{
    [SerializeField] public GameObject imgMultiplicador, imgSoldado, imgTanque, imgMissil;
    [SerializeField] public TMP_Text txTitulo, txDescricao;
    [SerializeField] [HideInInspector] public TipoCarta tipoCarta;
    [SerializeField][HideInInspector] public int valor;
    [SerializeField][HideInInspector] private ControleCartas cartasController;
    [SerializeField] private GameObject bordaSelecao;
    [SerializeField] [HideInInspector] public int idCarta;
    [SerializeField] [HideInInspector] public Jogador dono;
    
    public enum TipoCarta
    {
        MULTIPLICADOR, SOLDADO, TANQUE, MISSIL
    }

    public void criarCartaAleatoria(ControleCartas cartasControllerResponse, Jogador donoResponse)
    {
        cartasController = cartasControllerResponse;
        dono = donoResponse;
        idCarta = dono.cartas.Count;
        int tipo = Random.Range(0, 15);
        valor = 1;

        if (tipo == 0 || tipo == 1)
        {
            tipoCarta = TipoCarta.MULTIPLICADOR;
            valor = Random.Range(2, 4);
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Multiplicador" : "Multiplier";
        }
        else if (tipo == 2 || tipo == 3)
        {
            tipoCarta = TipoCarta.TANQUE;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Tanque" : "Tank";
        }
        else if (tipo == 4)
        {
            tipoCarta = TipoCarta.MISSIL;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Míssil" : "Missile";
        }
        else
        {
            tipoCarta = TipoCarta.SOLDADO;
            valor = Random.Range(1, 5);
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Soldados" : "Soldiers";
        }

        txDescricao.text = (tipoCarta == TipoCarta.MULTIPLICADOR ? "x" : "+") + valor;
        ativarImgPorTipo(tipoCarta);
    }

    private void ativarImgPorTipo(TipoCarta tipo)
    {
        imgMultiplicador.SetActive(TipoCarta.MULTIPLICADOR.Equals(tipo));
        imgSoldado.SetActive(TipoCarta.SOLDADO.Equals(tipo));
        imgTanque.SetActive(TipoCarta.TANQUE.Equals(tipo));
        imgMissil.SetActive(TipoCarta.MISSIL.Equals(tipo));
    }

    public void SelecionarToggleCartaParaTrocar()
    {
        if (!bordaSelecao.activeSelf)
        {
            cartasController.adicionarCartaNaListaParaTrocar(this);
            bordaSelecao.SetActive(true);
        }
        else
        {
            DeselecionarCartaParaTrocar();
        }
    }

    public void DeselecionarCartaParaTrocar()
    {
        bordaSelecao.SetActive(false);
        cartasController.removerCartaDaListaParaTrocar(this);
    }

    public void AtualizarCarta(TipoCarta tipoResponse, int valorResponse){
        if (tipoResponse == TipoCarta.MULTIPLICADOR)
        {
            txDescricao.text = "x" + valorResponse;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Multiplicador" : "Multiplier";
        }
        else if (tipoResponse == TipoCarta.TANQUE)
        {
            txDescricao.text = "+" + valorResponse;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Tanque" : "Tank";
        }
        else if (tipoResponse == TipoCarta.MISSIL)
        {
            txDescricao.text = "+" + valorResponse;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Míssil" : "Missile";
        }
        else
        {
            txDescricao.text = "+" + valorResponse;
            txTitulo.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Soldados" : "Soldiers";
        }
        ativarImgPorTipo(tipoResponse);
    }

    public void DesativarObjetoCartaPosAnim()
    {
        gameObject.SetActive(false);
    }

}
