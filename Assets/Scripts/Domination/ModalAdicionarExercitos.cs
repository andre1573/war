using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ModalAdicionarExercitos : MonoBehaviour
{

    [SerializeField][HideInInspector] public Territorio territorio, territorioBase, territorioDestino;
    [SerializeField] public GameObject botoesAguardeSuaVez, botoesDistribuicao, botoesAtaque, botoesRemanejo, botaoMudarFaseTurno, abasTipoExercitosPraRemanejar, ObjImagemSoldadoAbaAtaque, ObjImagemTanqueAbaAtaque, ObjImagemMissilAbaAtaque, ObjImageDistribuindo, ObjImageAtacando, ObjImageRemanejando;
    [SerializeField] public Button btAbaSoldados, btAbaTanques, btAbaMisseis, btAbaRemanejarSoldados, btAbaRemanejarTanques, btAbaDistribuirSoldados, btAbaDistribuirTanques;
    [SerializeField] public TMP_Text textNomeTerritorio, textNomeTerritorioAtaque, textQtdExercitosDisponiveisDistribuicao, textQtdExercitosDisponiveisRemanejo, txTerritorioBase, txTerritorioDestino, txContadorExercitosRemanejamento, txContadorExercitosDistribuicao;
    [SerializeField] public TMP_Text txDescricaoAguardandoJogador, txTanquesEstoque, txMisseisEstoque;
    [SerializeField] GameControllerDomination gameController;
    [SerializeField] [HideInInspector] public int qtdVezesRemanejou = 0;
    [SerializeField] [HideInInspector] private int contadorExercitosRemanejamento = 0, contadorExercitosDistribuicao = 0;
    [SerializeField][HideInInspector] public TipoAbaArmamentos tipoAbaDistribuicao, tipoAbaAtaque, tipoAbaRemanejo;
    public Slider sliderDistribuicao, sliderRemanejo;
   

    public enum TipoAbaArmamentos
    {
        Soldados,
        Tanques,
        Misseis
    }

    public void ReiniciarVariaveisPorTurno()
    {
        if (territorio != null) { territorio.AtualizarTextQtdExercitos(); territorio.imagem.color = territorio.dono.color; territorio = null; }
        if (territorioBase != null) { territorioBase.AtualizarTextQtdExercitos(); territorioBase.imagem.color = territorioBase.dono.color; setarTerritorioBase(null); }
        if (territorioDestino != null) { territorioDestino.AtualizarTextQtdExercitos(); territorioDestino.imagem.color = territorioDestino.dono.color; setarTerritorioDestino(null); }
        if (gameController.territorioAtacando != null) gameController.selecionarTerritorioAtacando(null);
        if (gameController.territorioDefendendo != null) gameController.territorioDefendendo = null;
        if (gameController.territorioRemanejoBase != null) gameController.selecionarTerritorioRemanejoBase(null);
        if (gameController.territorioRemanejoDestino != null) gameController.territorioRemanejoDestino = null;

        territorio = null;
        qtdVezesRemanejou = 0;
        setarTerritorioBase(null);
        setarTerritorioDestino(null);
        botoesDistribuicao.SetActive(false);
        botoesAtaque.SetActive(false);
        botoesRemanejo.SetActive(false);
        botaoMudarFaseTurno.SetActive(false);
        setContadorExercitosRemanejamento(0);
        SelecionarAbaSoldadosInFaseDistribuicao();
        SelecionarAbaSoldadosInFaseAtaque();
        SelecionarAbaSoldadosInFaseRemanejo();
    }

    public void setarTerritorioBase(Territorio territorioResponse)
    {
        territorioBase = territorioResponse;
        if(territorioResponse != null)
        {
            txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? territorioResponse.nomePortugues : territorioResponse.nomeIngles;
        }
        else
        {
            txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Selecione um territ�rio" : "Select a territory";
        }
    }

    private void setarTerritorioDestino(Territorio territorioResponse)
    {
        territorioDestino = territorioResponse;
        if (territorioResponse != null)
        {
            txTerritorioDestino.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? territorioResponse.nomePortugues : territorioResponse.nomeIngles;
        }
        else
        {
            txTerritorioDestino.text = "";
        }
    }

    public void setSliderDistribuicao(float valor)
    {
        int valorAux = (int)valor;
        sliderDistribuicao.value = valorAux;
        setContadorExercitosDistribuicao(valorAux);
    }

    public void setSliderRemanejo(float valor)
    {
        int valorAux = (int)valor;
        sliderRemanejo.value = valorAux;
        setContadorExercitosRemanejamento(valorAux);
    }

    public void setContadorExercitosRemanejamento(int valor)
    {
        contadorExercitosRemanejamento = valor;
        txContadorExercitosRemanejamento.text = valor +"";
        sliderRemanejo.value = valor;
    }

    public void setContadorExercitosDistribuicao(int valor)
    {
        contadorExercitosDistribuicao = valor;
        txContadorExercitosDistribuicao.text = valor + "";
        sliderDistribuicao.value = valor;
    }

    public void MenosUmExercitoDistribuicao()
    {
        if(contadorExercitosDistribuicao > 0)
        {
            setContadorExercitosDistribuicao(contadorExercitosDistribuicao - 1);
        }
    }

    public void MaisUmExercitoDistribuicao()
    {
        if (territorio == null) return;
        if (gameController.meuIndex == territorio.indexDono)
        {
            int qtdExercitosDisponiveisAux = gameController.jogadores[territorio.indexDono].qtdSoldadosDisponiveisNoRound - contadorExercitosDistribuicao;
            if (tipoAbaDistribuicao == TipoAbaArmamentos.Tanques) qtdExercitosDisponiveisAux = gameController.jogadores[territorio.indexDono].qtdTanquesDisponiveis - contadorExercitosDistribuicao;
            if (qtdExercitosDisponiveisAux > 0)
            {
                setContadorExercitosDistribuicao(contadorExercitosDistribuicao + 1);
            }
        }
    }

    public void ConfirmarDistribuicao()
    {
        if (contadorExercitosDistribuicao <= 0) return;
        if (tipoAbaDistribuicao == TipoAbaArmamentos.Soldados)
        {
            territorio.qtdSoldadosNoTerritorio += contadorExercitosDistribuicao;
            gameController.jogadores[territorio.indexDono].qtdSoldadosDisponiveisNoRound -= contadorExercitosDistribuicao;
        }
        else if (tipoAbaDistribuicao == TipoAbaArmamentos.Tanques)
        {
            territorio.qtdTanquesNoTerritorio += contadorExercitosDistribuicao;
            gameController.jogadores[territorio.indexDono].qtdTanquesDisponiveis -= contadorExercitosDistribuicao;
            gameController.soundsController.TocarSoundDistribuiuTanque();
            if(gameController.jogadores[territorio.indexDono].qtdTanquesDisponiveis == 0)
            {
                AbaParaDistribuirSoldados();
            }
        }
        territorio.AtualizarTextQtdExercitos();
        atualizarModalAdicionarExercitos();
        setContadorExercitosDistribuicao(0);
        if (gameController.jogadores[territorio.indexDono].qtdSoldadosDisponiveisNoRound == 0 )
        {
            botaoMudarFaseTurno.SetActive(true);
        }
        if (gameController.jogadores[gameController.meuIndex].qtdSoldadosDisponiveisNoRound <= 0 && gameController.jogadores[territorio.indexDono].qtdTanquesDisponiveis <= 0)
        {
            gameController.MudarFaseTurno();
        }
        gameController.cartasController.AtualizarInfoArmamentos(gameController.meuIndex, gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis, gameController.jogadores[gameController.meuIndex].qtdMisseisDisponiveis);
    }

    public void AbaParaDistribuirSoldados()
    {
        SelecionarAbaSoldadosInFaseDistribuicao();
        contadorExercitosDistribuicao = gameController.jogadores[gameController.meuIndex].qtdSoldadosDisponiveisNoRound;
        textQtdExercitosDisponiveisDistribuicao.text = contadorExercitosDistribuicao + "";
        btAbaDistribuirSoldados.interactable = false;
        btAbaDistribuirTanques.interactable = true;
        setContadorExercitosDistribuicao(0);
    }

    public void AbaParaDistribuirTanques()
    {
        SelecionarAbaTanquesInFaseDistribuicao();
        contadorExercitosDistribuicao = gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis;
        textQtdExercitosDisponiveisDistribuicao.text = contadorExercitosDistribuicao + "";
        btAbaDistribuirSoldados.interactable = true;
        btAbaDistribuirTanques.interactable = false;
        setContadorExercitosDistribuicao(0);
    }

    public void atualizarModalAdicionarExercitos()
    {
        txTanquesEstoque.text = gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis + "";
        txMisseisEstoque.text = gameController.jogadores[gameController.meuIndex].qtdMisseisDisponiveis + "";
        if (tipoAbaDistribuicao.Equals(TipoAbaArmamentos.Soldados))
        {
            if(gameController.jogadores != null && gameController.jogadores.Count > 0)
            {
                textQtdExercitosDisponiveisDistribuicao.text = gameController.jogadores[gameController.meuIndex].qtdSoldadosDisponiveisNoRound + "";
                sliderDistribuicao.maxValue = gameController.jogadores[gameController.meuIndex].qtdSoldadosDisponiveisNoRound;
            }
        }
        else
        {
            if (gameController.jogadores != null && gameController.jogadores.Count > 0)
            {
                textQtdExercitosDisponiveisDistribuicao.text = gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis + "";
                sliderDistribuicao.maxValue = gameController.jogadores[gameController.meuIndex].qtdTanquesDisponiveis;
            }
        }
    }

    public void RemanejarMenosUmExercito()
    {
        if(contadorExercitosRemanejamento > 0)
        {
            setContadorExercitosRemanejamento(contadorExercitosRemanejamento-1);
        }
    }
    public void RemanejarMaisUmExercito()
    {
        if (territorioBase == null || territorioDestino == null) return;
        bool isPodeRemanejarExercitosNoTerritorio = false;
        if(TipoAbaArmamentos.Tanques == tipoAbaRemanejo)
        {
            isPodeRemanejarExercitosNoTerritorio = territorioBase.qtdTanquesNoTerritorio - contadorExercitosRemanejamento >= 1;
        }
        else //SOLDADOS
        {
            isPodeRemanejarExercitosNoTerritorio = territorioBase.qtdSoldadosNoTerritorio - contadorExercitosRemanejamento > 1;
        }
        if (isPodeRemanejarExercitosNoTerritorio)
        {
           setContadorExercitosRemanejamento(contadorExercitosRemanejamento + 1);
        }
        else
        {
            gameController.textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Quantidade de ex�rcito desse tipo insuficiente para remanejar!" : "Insufficient amount of such army to redeploy!";
        }
    }

    public void ConfirmarRemanejo()
    {
        string nomeFronteira = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? territorioBase.nomeIngles + territorioDestino.nomeIngles : territorioBase.nomePortugues + territorioDestino.nomePortugues;
        if (qtdVezesRemanejou < 3)
        {
            if(contadorExercitosRemanejamento > 0)
            {
                if (TipoAbaArmamentos.Tanques == tipoAbaRemanejo)
                {
                    territorioBase.qtdTanquesNoTerritorio -= contadorExercitosRemanejamento;
                    territorioDestino.qtdTanquesNoTerritorio += contadorExercitosRemanejamento;

                    setContadorExercitosRemanejamento(0);
                    sliderRemanejo.maxValue = territorioBase.qtdTanquesNoTerritorio;
                    textQtdExercitosDisponiveisRemanejo.text = sliderRemanejo.maxValue + "";
                    territorioBase.informacoesTerritorio.textQtdExercitos.text = territorioBase.qtdTanquesNoTerritorio + "";
                    territorioDestino.informacoesTerritorio.textQtdExercitos.text = territorioDestino.qtdTanquesNoTerritorio + "";
                }
                else //SOLDADOS
                {
                    territorioBase.qtdSoldadosNoTerritorio -= contadorExercitosRemanejamento;
                    territorioDestino.qtdSoldadosNoTerritorio += contadorExercitosRemanejamento;

                    setContadorExercitosRemanejamento(0);
                    sliderRemanejo.maxValue = territorioBase.qtdSoldadosNoTerritorio - 1;
                    textQtdExercitosDisponiveisRemanejo.text = sliderRemanejo.maxValue + "";
                    territorioBase.informacoesTerritorio.textQtdExercitos.text = territorioBase.qtdSoldadosNoTerritorio + "";
                    territorioDestino.informacoesTerritorio.textQtdExercitos.text = territorioDestino.qtdSoldadosNoTerritorio + "";
                }

                territorioBase.AtualizarTextQtdExercitos();
                territorioBase.imagem.color = territorioBase.dono.color;
                territorioDestino.AtualizarTextQtdExercitos();
                territorioDestino.imagem.color = territorioDestino.dono.color;
                setarTerritorioBase(null);
                setarTerritorioDestino(null);
                FinalizarRemanejo();
                qtdVezesRemanejou++;

                if(qtdVezesRemanejou >= 3)
                {
                    FinalizarFaseTurno();
                }
            }
            else
            {
                gameController.textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Informe a quantidade de ex�rcitos que deseja remanejar!" : "Enter the number of armies you want to redeploy!";
            }
        }
        else
        {
            gameController.textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "S� � poss�vel remanejar 3 vezes por turno!" : "You can only relocate 3 times per turn!";
        }
    }

    public void FinalizarFaseTurno()
    {
        if(gameController.indexJogadorAtual == gameController.meuIndex)
        {
            ReiniciarVariaveisPorTurno();
            gameController.MudarFaseTurno();
        }
    }

    public void FinalizarRemanejo()
    {
        territorio = null;
        setarTerritorioBase(null);
        setarTerritorioDestino(null);
        gameController.selecionarTerritorioRemanejoBase(null);
        gameController.territorioRemanejoDestino = null;
        txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Selecione um territ�rio" : "Select a territory";
        botoesRemanejo.SetActive(false);
    }

    public void SelecionarAbaSoldadosInFaseDistribuicao()
    {
        tipoAbaDistribuicao = TipoAbaArmamentos.Soldados;
        btAbaDistribuirSoldados.interactable = false;
        btAbaDistribuirTanques.interactable = true;
        atualizarModalAdicionarExercitos();
    }

    public void SelecionarAbaTanquesInFaseDistribuicao()
    {
        tipoAbaDistribuicao = TipoAbaArmamentos.Tanques;
        btAbaDistribuirSoldados.interactable = true;
        btAbaDistribuirTanques.interactable = false;
        atualizarModalAdicionarExercitos();
    }

    public void SelecionarAbaSoldadosInFaseAtaque()
    {
        btAbaSoldados.interactable = false;
        btAbaTanques.interactable = true;
        btAbaMisseis.interactable = true;
        ObjImagemSoldadoAbaAtaque.SetActive(true);
        ObjImagemTanqueAbaAtaque.SetActive(false);
        ObjImagemMissilAbaAtaque.SetActive(false);
        tipoAbaAtaque = TipoAbaArmamentos.Soldados;
    }

    public void SelecionarAbaTanquesInFaseAtaque()
    {
        btAbaSoldados.interactable = true;
        btAbaTanques.interactable = false;
        btAbaMisseis.interactable = true;
        ObjImagemSoldadoAbaAtaque.SetActive(false);
        ObjImagemTanqueAbaAtaque.SetActive(true);
        ObjImagemMissilAbaAtaque.SetActive(false);
        tipoAbaAtaque = TipoAbaArmamentos.Tanques;
    }

    public void SelecionarAbaMisseisInFaseAtaque()
    {
        if(gameController.jogadores[gameController.meuIndex].qtdMisseisDisponiveis > 0)
        {
            btAbaSoldados.interactable = true;
            btAbaTanques.interactable = true;
            btAbaMisseis.interactable = false;
            ObjImagemSoldadoAbaAtaque.SetActive(false);
            ObjImagemTanqueAbaAtaque.SetActive(false);
            ObjImagemMissilAbaAtaque.SetActive(true);
            tipoAbaAtaque = TipoAbaArmamentos.Misseis;
            gameController.soundsController.TocarSoundPreparandoAtkMissil();
        }
    }

    public void SelecionarAbaSoldadosInFaseRemanejo()
    {
        btAbaRemanejarSoldados.interactable = false;
        btAbaRemanejarTanques.interactable = true;
        tipoAbaRemanejo = TipoAbaArmamentos.Soldados;
        setContadorExercitosRemanejamento(0);
        if(gameController.territorioRemanejoBase != null)
        {
            sliderRemanejo.maxValue = gameController.territorioRemanejoBase.qtdSoldadosNoTerritorio - 1;
            textQtdExercitosDisponiveisRemanejo.text = sliderRemanejo.maxValue + "";
        }
    }

    public void SelecionarAbaTanquesInFaseRemanejo()
    {
        btAbaRemanejarTanques.interactable = false;
        btAbaRemanejarSoldados.interactable = true;
        tipoAbaRemanejo = TipoAbaArmamentos.Tanques;
        setContadorExercitosRemanejamento(0);
        if (gameController.territorioRemanejoBase != null)
        {
            sliderRemanejo.maxValue = gameController.territorioRemanejoBase.qtdTanquesNoTerritorio;
            textQtdExercitosDisponiveisRemanejo.text = sliderRemanejo.maxValue + "";
        }
    }

}
