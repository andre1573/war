using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dado : MonoBehaviour
{

    public TMP_Text txValorDado;

    public void AtualizarValorDado(int valor)
    {
        txValorDado.text = valor + "";
        if (valor > 0)
        {
            this.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }

}
