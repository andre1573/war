using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class Territorio : MonoBehaviourPunCallbacks
{

    [SerializeField] public int idTerritorio;
    [SerializeField] public string nomePortugues, nomeIngles;
    [SerializeField][HideInInspector] public Jogador dono;
    [SerializeField] [HideInInspector] public int indexDono, qtdSoldadosNoTerritorio, qtdTanquesNoTerritorio;
    [SerializeField] public InformacoesTerritorio informacoesTerritorio;
    [SerializeField] public Continente continente;

    public Image imagem;

    [SerializeField] public ModalAdicionarExercitos modalAdicionarExercitos;
    [SerializeField] public Territorio[] fronteirasTerritorios;
    [SerializeField] public GameControllerDomination controller;
    [HideInInspector] PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        qtdSoldadosNoTerritorio = 1;
        qtdTanquesNoTerritorio = 0;
        informacoesTerritorio.textQtdExercitos.text = qtdSoldadosNoTerritorio + "";
        informacoesTerritorio.textQtdTanques.text = qtdTanquesNoTerritorio + "";
        informacoesTerritorio.textNome.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? nomePortugues : nomeIngles;
    }

    public void setarDonoTerritorio(Jogador jogadorDono)
    {
        indexDono = jogadorDono.index;
        dono = jogadorDono;
        setarCorDoTerritorioPorCorDoDono();
        if(!jogadorDono.territorios.Contains(this)) jogadorDono.territorios.Add(this);
        controller.listaPlayersItemInGame[jogadorDono.index].txInfoQtdTerritorios.text = jogadorDono.territorios.Count + "";
    }

    public void trocarDonoTerritorio(Jogador donoAntigo, Jogador donoNovo)
    {
        indexDono = donoNovo.index;
        dono = donoNovo;
        setarCorDoTerritorioPorCorDoDono();
        if (donoAntigo.territorios.Contains(this)) controller.jogadores[donoAntigo.index].territorios.Remove(this);
        if (!donoNovo.territorios.Contains(this)) controller.jogadores[donoNovo.index].territorios.Add(this);
        controller.listaPlayersItemInGame[donoAntigo.index].txInfoQtdTerritorios.text = donoAntigo.territorios.Count + "";
        controller.listaPlayersItemInGame[donoNovo.index].txInfoQtdTerritorios.text = donoNovo.territorios.Count + "";
    }

    public void setarQtdSoldadosNoTerritorio(int qtd)
    {
        qtdSoldadosNoTerritorio = qtd;
        informacoesTerritorio.textQtdExercitos.text = qtdSoldadosNoTerritorio + "";
    }

    public void setarQtdTanquesNoTerritorio(int qtd)
    {
        qtdTanquesNoTerritorio = qtd;
        informacoesTerritorio.textQtdTanques.text = qtdTanquesNoTerritorio + "";
    }

    private void setarCorDoTerritorioPorCorDoDono()
    {
        if (dono.color != null)
        {
            imagem.color = dono.color;
            informacoesTerritorio.imgSoldado.color = dono.color;
            informacoesTerritorio.imgTanque.color = dono.color;
        }
    }

    private void AbrirModalBotoesAdicionarExercitos()
    {
        if(indexDono == controller.meuIndex)
        {
            modalAdicionarExercitos.botoesDistribuicao.SetActive(true);
            if(modalAdicionarExercitos.territorio!=null) modalAdicionarExercitos.territorio.imagem.color = dono.color;
            modalAdicionarExercitos.territorio = this;
            this.imagem.color = controller.diminuirBrilhoDaCor(dono.color);
            modalAdicionarExercitos.textNomeTerritorio.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? nomeIngles : nomePortugues;
        }
    }

    public void AtualizarTextQtdExercitos()
    {
        PV.RPC("RPC_AtualizarExercitosNoTerritorio", RpcTarget.All, qtdSoldadosNoTerritorio, qtdTanquesNoTerritorio);
    }

    [PunRPC]
    void RPC_AtualizarExercitosNoTerritorio(int qtdSoldadosResponde, int qtdTanquesResponse)
    {
        qtdSoldadosNoTerritorio = qtdSoldadosResponde;
        informacoesTerritorio.textQtdExercitos.text = qtdSoldadosNoTerritorio + "";
        qtdTanquesNoTerritorio = qtdTanquesResponse;
        if(informacoesTerritorio.textQtdTanques !=null) informacoesTerritorio.textQtdTanques.text = qtdTanquesNoTerritorio + "";
        if(qtdTanquesNoTerritorio > 0)
        {
            informacoesTerritorio.objTanque.SetActive(true);
        }
        else
        {
            informacoesTerritorio.objTanque.SetActive(false);
        }
    }

    public void SelecionarTerritorio()
    {
        if(controller.tipoFaseTurno == GameControllerDomination.TipoFaseTurno.DISTRIBUICAO)
        {
            AbrirModalBotoesAdicionarExercitos();
        }
        else if (controller.tipoFaseTurno == GameControllerDomination.TipoFaseTurno.ATAQUE)
        {
            setarTerritorioAtaqueDefesa();
        }
        else if(controller.tipoFaseTurno == GameControllerDomination.TipoFaseTurno.REMANEJO)
        {
            setarTerritorioRemanejoBaseDestino();
        }
    }

    private void setarTerritorioAtaqueDefesa()
    {
        if (dono.index == controller.meuIndex)
        {
            if (modalAdicionarExercitos.territorio != null) modalAdicionarExercitos.territorio.imagem.color = modalAdicionarExercitos.territorio.dono.color;
            if (controller.territorioAtacando!=null) controller.territorioAtacando.imagem.color = controller.territorioAtacando.dono.color;
            controller.selecionarTerritorioAtacando(this);
            controller.territorioDefendendo = null;
            modalAdicionarExercitos.textNomeTerritorioAtaque.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? nomeIngles : nomePortugues;
            modalAdicionarExercitos.botoesAtaque.SetActive(true);
        }
        else
        {
            if (controller.territorioAtacando != null) controller.territorioAtacando.informacoesTerritorio.marcacaoFronteiras.SetActive(false);
            if (ModalAdicionarExercitos.TipoAbaArmamentos.Misseis.Equals(modalAdicionarExercitos.tipoAbaAtaque))
            {
                controller.territorioDefendendo = this;
                controller.AtacarTerritorioComMissil();
            }
            else
            {
                if (controller.territorioAtacando != null)
                {
                    if (possuiFronteira(controller.territorioAtacando.fronteirasTerritorios))
                    {
                        controller.territorioDefendendo = this;
                        controller.AtacarTerritorio();
                    }
                }
            }
        }
    }

    private void setarTerritorioRemanejoBaseDestino()
    {
        if (indexDono == controller.meuIndex)
        {
            if(controller.territorioRemanejoBase == null || controller.territorioRemanejoDestino != null)
            {
                setarTerritorioRemanejoBase();
            }
            else
            {
                if(this.idTerritorio == controller.territorioRemanejoBase.idTerritorio)
                {
                    resetarTerritorioRemanejoBase();
                }
                else
                {
                    if (possuiFronteira(controller.territorioRemanejoBase.fronteirasTerritorios))
                    {
                        string nomeFronteira = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? controller.territorioRemanejoBase.nomeIngles + this.nomeIngles : controller.territorioRemanejoBase.nomePortugues + this.nomePortugues;
                        if (modalAdicionarExercitos.qtdVezesRemanejou < 3)
                        {
                            controller.territorioRemanejoDestino = this;
                            AbrirModalBotoesRemanejoExercitos();
                        }
                        else
                        {
                            controller.textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "S� � poss�vel remanejar 3 vezes por turno!" : "You can only relocate 3 times per turn!";
                        }
                    }
                    else
                    {
                        setarTerritorioRemanejoBase();
                    }
                }
            }
        }
    }

    private void setarTerritorioRemanejoBase()
    {
        if (this.qtdSoldadosNoTerritorio <= 1 && this.qtdTanquesNoTerritorio <= 0)
        {
            controller.textMensagem.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Esse territ�rio n�o possui quantidade de tropas suficiente para remanejar" : "This territory does not have enough troops to redeploy";
            return;
        }
        if (controller.territorioRemanejoBase != null) controller.territorioRemanejoBase.imagem.color = dono.color;
        if (controller.territorioRemanejoDestino != null) controller.territorioRemanejoDestino.imagem.color = dono.color;
        controller.selecionarTerritorioRemanejoBase(this);
        this.imagem.color = controller.diminuirBrilhoDaCor(dono.color);
        controller.territorioRemanejoDestino = null;
        modalAdicionarExercitos.txTerritorioDestino.text = "";
        modalAdicionarExercitos.txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? nomeIngles : nomePortugues;
        modalAdicionarExercitos.setContadorExercitosRemanejamento(0);
        FecharModalBotoesRemanejoExercitos();
    }

    private void resetarTerritorioRemanejoBase()
    {
        if (controller.territorioRemanejoBase != null) controller.territorioRemanejoBase.imagem.color = dono.color;
        if(controller.territorioRemanejoDestino != null) controller.territorioRemanejoDestino.imagem.color = dono.color;
        modalAdicionarExercitos.txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? "Selecione um territ�rio" : "Select a territory";
        modalAdicionarExercitos.txTerritorioDestino.text = "";
        controller.territorioRemanejoDestino = null;
        controller.selecionarTerritorioRemanejoBase(null);
        modalAdicionarExercitos.setContadorExercitosRemanejamento(0);
        FecharModalBotoesRemanejoExercitos();
    }

    private void AbrirModalBotoesRemanejoExercitos()
    {
        if (indexDono == controller.meuIndex)
        {
            modalAdicionarExercitos.territorio = this;
            modalAdicionarExercitos.territorioBase = controller.territorioRemanejoBase;
            modalAdicionarExercitos.territorioDestino = controller.territorioRemanejoDestino;
            modalAdicionarExercitos.textNomeTerritorio.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? nomeIngles : nomePortugues;
            modalAdicionarExercitos.txTerritorioBase.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? controller.territorioRemanejoBase.nomeIngles : controller.territorioRemanejoBase.nomePortugues;
            modalAdicionarExercitos.txTerritorioDestino.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 0 ? controller.territorioRemanejoDestino.nomeIngles : controller.territorioRemanejoDestino.nomePortugues;
            modalAdicionarExercitos.SelecionarAbaSoldadosInFaseRemanejo();
            modalAdicionarExercitos.botoesRemanejo.SetActive(true);
        }
    }

    private void FecharModalBotoesRemanejoExercitos()
    {
        modalAdicionarExercitos.botoesRemanejo.SetActive(false);
    }

    private bool possuiFronteira(Territorio[] fronteiras)
    {
        foreach(Territorio territorio in fronteiras)
        {
            if(territorio.nomeIngles == nomeIngles)
            {
                return true;
            }
        }
        return false;
    }

}
