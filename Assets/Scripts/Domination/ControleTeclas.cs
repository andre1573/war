using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControleTeclas : MonoBehaviour
{

    public Transform continentesObj;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            float scale = continentesObj.localScale.x + 0.2f;
            if (scale > 2) scale = 2;
            continentesObj.localScale = new Vector3(scale, scale, scale);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            float scale = continentesObj.localScale.x - 0.2f;
            if (scale < 1) scale = 1;
            continentesObj.localScale = new Vector3(scale, scale, scale);
        }
    }
}
