using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dicas : MonoBehaviour
{
    public InfoDicas[] dicas;
    public TMPro.TMP_Text textField;
    private float timeSinceUpdate;
    public float updateTime;
    // Start is called before the first frame update
    void Start()
    {
        int i = Random.Range(0,dicas.Length);
        textField.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1? dicas[i].portugues : dicas[i].ingles;
        timeSinceUpdate = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= timeSinceUpdate+updateTime){
            Atualizar();
            timeSinceUpdate = Time.time;
        }
    }

    public void Atualizar(){
        int i = Random.Range(0, dicas.Length);
        textField.text = PlayerPrefs.GetInt("INDEXIDIOMA") == 1 ? dicas[i].portugues : dicas[i].ingles;
    }
}
