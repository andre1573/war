using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class ColocacaoFimDeJogo : MonoBehaviour
{

    public TMP_Text text, txTempo;
    public GameObject canvasVictory, canvasVictoryCompetitive, canvasLose, canvasLoseCompetitive, botoesControleMaster, botoesFun;
    private bool preencheu = false;
    public AudioSource soundVictory, soundLose;
    PhotonView PV;


    void Awake()
    {
        preencheu = false;
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        if (!preencheu)
        {
            PreencherTextColocacao();
        }
        if (PhotonNetwork.CurrentLobby.Name != "competitive")
        {
            botoesFun.SetActive(true);
            if (PhotonNetwork.IsMasterClient)
            {
                botoesControleMaster.SetActive(true);
            }
            else
            {
                botoesControleMaster.SetActive(false);
            }
        }
        else
        {
            botoesFun.SetActive(false);
            botoesControleMaster.SetActive(false);
        }
    }

    private void PreencherTextColocacao()
    {
        preencheu = true;
        text.text = "" + PlayerPrefs.GetString("1col") + "";
        txTempo.text = "Time: "+PlayerPrefs.GetInt("1coltime") + " s";
        Debug.Log("Id vencedor: " + PlayerPrefs.GetString("vId") + "   --- Meu id: " + PhotonNetwork.LocalPlayer.UserId);
        if (PlayerPrefs.GetString("vId") == PhotonNetwork.LocalPlayer.UserId)
        {
            soundVictory.Play();
            canvasVictory.SetActive(true);
            if (PhotonNetwork.CurrentLobby.Name == "competitive")
            {
                canvasVictoryCompetitive.SetActive(true);
            }
            else
            {
                canvasVictoryCompetitive.SetActive(false);
            }
        }
        else
        {
            soundLose.Play();
            canvasLose.SetActive(true);
            if (PhotonNetwork.CurrentLobby.Name == "competitive")
            {
                canvasLoseCompetitive.SetActive(true);
            }
            else
            {
                canvasLoseCompetitive.SetActive(false);
            }
        }
    }


}
