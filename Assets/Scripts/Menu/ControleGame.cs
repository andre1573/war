using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControleGame : MonoBehaviour
{
    public GameObject hudBotoesMenuInGame;
    // Start is called before the first frame update
    void Start()
    {
        hudBotoesMenuInGame.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Escape))
        {
            toogleHudBotoesMenuInGame();
        }
        if (hudBotoesMenuInGame.activeSelf)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }

    public void continueGame(){
        hudBotoesMenuInGame.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void abrirMenuSettings()
    {
        //falta fazer
    }

    private void toogleHudBotoesMenuInGame()
    {
        if (hudBotoesMenuInGame.activeSelf)
        {
            hudBotoesMenuInGame.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            hudBotoesMenuInGame.SetActive(true);
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
      
       
    }

}
