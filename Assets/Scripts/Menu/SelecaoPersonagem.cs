using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelecaoPersonagem : MonoBehaviour
{

    public Image fotoPersonagem;
    public List<GameObject> skins;
    public Button btLeft, btRight;
    private List<bool> personagensPossui;

    private int index;
    SaveManager saveManager;


    private void Awake()
    {
        saveManager = GameObject.FindGameObjectWithTag("SaveManager").GetComponent<SaveManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        string skinkey = PlayerPrefs.GetString("personagemskin");
        if (saveManager.personagens[(skinkey)].Equals(1)) { 
            index = int.Parse(skinkey); //pega o ultimo digito que � igual a um numero ; //mudar pq qdo tive rmais de 10 skins vai dar erro
        }
        else
        {
            index = 0;
            PlayerPrefs.SetString("personagemskin", "0");
        }
        AtivarGameobjectSkinSelecionada(index);
        updatePersonagens();
    }

    private void AtivarGameobjectSkinSelecionada(int index)
    {
        Debug.Log("Index: " + index);
        for(int i = 0; i < skins.Count; i++)
        {
            if (i == index)
            {
                skins[i].gameObject.SetActive(true);
                Debug.Log("Ativou skin" + i);
            }
            else
            {
                skins[i].gameObject.SetActive(false);
                Debug.Log("Desativou skin" + i);
            }
        }
    }

    public void updatePersonagens()
    {
        personagensPossui = new List<bool>();
        int qtdPossui = 0;
        for(int i = 0; i < skins.Count; i++)
        {
            bool jaPossui = (saveManager.personagens[""+i].Equals(1));
            if (jaPossui) qtdPossui++;
            personagensPossui.Add(jaPossui);
        }
    }

    public void rightSelection()
    {
        index++;
        bool tem = false;
        while (!tem || index == 0)
        {
            if (index >= skins.Count) index = 0;
            if (saveManager.personagens["" + index].Equals(1))
            {
                tem = true;
                break;
            }
            else
            {
                index++;
            }
        }

        AtivarGameobjectSkinSelecionada(index);
        PlayerPrefs.SetString("personagemskin", ""+index);
    }

    public void leftSelection()
    {
        index--;
        bool tem = false;
        while (!tem || index == 0)
        {
            if (index < 0) index = skins.Count - 1;
            if (saveManager.personagens[("" + index)].Equals(1))
            {
                tem = true;
                break;
            }
            else
            {
                index--;
            }
        }

        AtivarGameobjectSkinSelecionada(index);
        PlayerPrefs.SetString("personagemskin", "" + index);
    }

}
