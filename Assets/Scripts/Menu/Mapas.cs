using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;


public class Mapas : MonoBehaviourPunCallbacks
{
    [SerializeField] public Sprite[] imgMapas;
    [SerializeField] public string[] txMapas;
    public Image fotoMapa;
    public TMP_Text nomeMapa;
    [HideInInspector] public int index;
    public Button buttonLeftMap, buttonRightMap;
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    void Start()
    {
        index = 0;
        fotoMapa.sprite = imgMapas[index];
        nomeMapa.text = txMapas[index];
        if(!PhotonNetwork.IsMasterClient)
        {
            buttonLeftMap.gameObject.SetActive(false);
            buttonRightMap.gameObject.SetActive(false);
        }
        else
        {
            buttonLeftMap.gameObject.SetActive(true);
            buttonRightMap.gameObject.SetActive(true);
        }
    }

    private void LateUpdate()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            buttonLeftMap.gameObject.SetActive(false);
            buttonRightMap.gameObject.SetActive(false);
        }
        else
        {
            buttonLeftMap.gameObject.SetActive(true);
            buttonRightMap.gameObject.SetActive(true);
        }
    }

    public void rightSelection()
    {
        index++;
        if (index >= imgMapas.Length) index = 0;
        PV.RPC("RPC_AtualizarIndexMapas", RpcTarget.All, index);
    }

    public void leftSelection()
    {
        index--;
        if (index < 0) index = imgMapas.Length - 1;
        PV.RPC("RPC_AtualizarIndexMapas", RpcTarget.All, index);
    }

    [PunRPC]
    void RPC_AtualizarIndexMapas(int index)
    {
        fotoMapa.sprite = imgMapas[index];
        nomeMapa.text = txMapas[index];
    }

}
