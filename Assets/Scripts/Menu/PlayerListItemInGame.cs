﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListItemInGame : MonoBehaviourPunCallbacks
{
	[SerializeField] public Image imgCor;
	[SerializeField] public RawImage imgBoardInfos;
	[SerializeField] public TMP_Text textNomeJogador, textCor, txInfoNomeJogador, txInfoQtdTerritorios, txInfoQtdCartas, txInfoQtdTanques, txInfoQtdMisseis;
	[SerializeField] GameObject bordaJogadorAtual, informacoesExtrasCadaJogador;
	[SerializeField][HideInInspector] Jogador jogador;


    public void SetUp(Jogador jogador)
	{
		textNomeJogador.text = jogador.nickname;
		txInfoNomeJogador.text = jogador.nickname;
		textCor.text = jogador.cor;
		imgCor.color = jogador.color;
		imgBoardInfos.color = jogador.color;
	}

	public void ToggleBordaJogadorAtual(bool isAtiva)
    {
		bordaJogadorAtual.SetActive(isAtiva);
    }

	public void DesativarInfo(){
		informacoesExtrasCadaJogador.SetActive(false);
	}

}