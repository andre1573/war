using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InfoDicas
{
    public string portugues;
    public string ingles;
}
