using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;

public class ShopPersonagem : MonoBehaviour
{

	private string catalogVersion = "Catalogo Deathrun Guys";
	public string itemIndex, itemName, itemDesc;
	public int itemPrice;
	public GameObject hudJaPossui;
	public Button buttonBuyPersonagem;
	public SelecaoPersonagem selecaoPersonagem;
	public TMP_Text txStatus;

	public TMP_Text txItemName, txItemCost;
	private bool jaPossui = false;
	SaveManager saveManager;

	private void Start()
	{
		saveManager = GameObject.FindGameObjectWithTag("SaveManager").GetComponent<SaveManager>();
		txItemName.text = itemName;
		txItemCost.text = itemPrice+"";
		if(saveManager.personagens[""+itemIndex].Equals(1))
        {
			jaPossui = true;
        }
	}

    private void LateUpdate()
    {
		if (saveManager.personagens.ContainsKey("" + itemIndex) && saveManager.personagens["" + itemIndex].Equals(1))
		{
			jaPossui = true;
		}
		if (jaPossui)
        {
			hudJaPossui.SetActive(true);
			buttonBuyPersonagem.gameObject.SetActive(false);
        }
        else
        {
			if(hudJaPossui.activeSelf) hudJaPossui.SetActive(false);
			buttonBuyPersonagem.gameObject.SetActive(true);
		}
    }

    public void BuyPersonagem()
	{
		if (saveManager.personagens["" + itemIndex].Equals(1)) return; //ja possui o personagem
		PurchaseItemRequest request = new PurchaseItemRequest();
		request.CatalogVersion = catalogVersion;
		request.ItemId = itemIndex;
		request.VirtualCurrency = "GE";
		request.Price = itemPrice;
		PlayFabClientAPI.PurchaseItem(request, OnDataSend, OnError);
	}

	void OnDataSend(PurchaseItemResult result) //save progress ok
	{
		Debug.Log("Personagem obtido com sucesso");
		GameObject sm = GameObject.FindGameObjectWithTag("SaveManager");
		sm.GetComponent<SaveManager>().GetProgress();
		sm.GetComponent<SaveManager>().SaveProgress();
		sm.GetComponent<SaveManager>().GetInventory();
		PlayerPrefs.SetInt("GEMS", PlayerPrefs.GetInt("GEMS") - itemPrice);
		if (!saveManager.personagens.ContainsKey("" + itemIndex))
		{
			saveManager.personagens.Add("" + itemIndex, 0);
		}
		saveManager.personagens["" + itemIndex] = 1;
		jaPossui = true;
		selecaoPersonagem.updatePersonagens();
		txStatus.color = Color.green;
		txStatus.text = "Sucess";
	}
	void OnError(PlayFabError error)
	{
		txStatus.color = Color.red;
		txStatus.text = error.ErrorMessage;
		Debug.LogError("Erro ao obter personagem: " + error.ErrorMessage);
	}

}
