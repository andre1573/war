using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityEngine.Video;

[RequireComponent(typeof(PhotonView))]
public class ColocacaoControle : MonoBehaviourPunCallbacks
{

	[HideInInspector] public ColocacaoPlayer primeiroColocado;
	PhotonView PV;
    private bool iniciouCorrida = false, iniciouContagem = false, tocouSoundCountdown = false, pausarCronometro = false;
    [SerializeField] bool inModoBunnyhop = false;
    [SerializeField] PistasBunnyhopControle pistasBunnyhopControle;
    public float tempoContagemEsperar = 6;
    private float contagemEsperar = 6, contagemRegressiva = 10;
    [HideInInspector] public float cronometroPartida = 0;
    [HideInInspector] public float score = 0;
    public GameObject paredeLargada, canvasFundoVideo, canvasScoreBunnyhop;
    public TMP_Text txContagemLargada, txCronometro, txScoreBunnyhop;
    public AudioSource soundCountdown;
    public Animation abrirPortaoE, abrirPortaoD;
    public VideoPlayer videoPlayer;
    private Camera cameraPlayer;
    private PlayerController player;


    private void Awake()
    {
        setarCameraVideoPlayer();
        if (PhotonNetwork.CurrentLobby.Name == "competitive")
        {
            PlayerPrefs.SetInt("inpartida", 1);
            Debug.Log("setou in partida 1");
        }
    }

    private void Start()
    {
        PV = GetComponent<PhotonView>();
        IniciarVariaveisDaCorrida();
    }

    private void IniciarVariaveisDaCorrida()
    {
        paredeLargada.SetActive(true);
        txContagemLargada.gameObject.SetActive(true);
        contagemEsperar = tempoContagemEsperar;
        if(inModoBunnyhop)
        {
            contagemRegressiva = 5;
        }
        else
        {
            contagemRegressiva = 10;
        }
        iniciouCorrida = false;
        iniciouContagem = false;
        tocouSoundCountdown = false;
        cronometroPartida = 0;
        score = 0;
        txCronometro.text = "";
        pausarCronometro = false;
        setarCameraVideoPlayer();
        if(pistasBunnyhopControle!=null) pistasBunnyhopControle.ReiniciarPistas();
    }

    private void Update()
    {
        if (inModoBunnyhop && player == null)
        {
            GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
            if(objPlayer != null)
            {
                player = objPlayer.GetComponent<PlayerController>();
            }
        }
        setarCameraVideoPlayer();
        if (canvasScoreBunnyhop != null && canvasScoreBunnyhop.activeSelf)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            if (Input.GetButton("joystick0"))
            {
                PlayAgainModoBunnyhop();
            }
        }
        if (iniciouCorrida)
        {
            if(cameraPlayer!=null && !cameraPlayer.isActiveAndEnabled)cameraPlayer.enabled = true;
            if (!pausarCronometro)
            {
                cronometroPartida += Time.deltaTime;
                if (player != null)
                {
                    score += (Time.deltaTime * player.hiddenVars.GetFloat("velBunnyhop"));
                    Debug.Log("Score: " + score);
                }
            }
            if (inModoBunnyhop)
            {
                txCronometro.text = "Score: " + (int)score + "";
            }
            else
            {
                txCronometro.text = "Time: " + (int)cronometroPartida + " s";
            }
            if(paredeLargada.activeSelf) paredeLargada.SetActive(false);
        }
        else
        {
            if (!iniciouContagem)
            {
                txContagemLargada.text = ""; //WAITING!
                if (contagemEsperar <= 0 && PhotonNetwork.IsMasterClient)
                {
                    Debug.Log("Tempo de espera acabou, Iniciando partida");
                    PV.RPC("RPC_IniciarContagemRegressiva", RpcTarget.All);
                }
                contagemEsperar -= Time.deltaTime;
            }
            else //Iniciou Contagem
            {
                if (cameraPlayer != null && !cameraPlayer.isActiveAndEnabled) cameraPlayer.enabled = true;
                if (PhotonNetwork.IsMasterClient && contagemRegressiva <= 0)
                {
                    PV.RPC("RPC_IniciarCorrida", RpcTarget.All, cronometroPartida);
                }
                if (((int)contagemRegressiva) <= 0)
                {
                    txContagemLargada.text = "GO!";
                }
                else
                {
                    txContagemLargada.text = (int)contagemRegressiva + "";
                    if(!tocouSoundCountdown && (int)contagemRegressiva <= 3)
                    {
                        soundCountdown.Play();
                        tocouSoundCountdown = true;
                    }
                }
                contagemRegressiva -= Time.deltaTime;
            }
        }
    }

    private void setarCameraVideoPlayer()
    {
        if (cameraPlayer == null)
        {
            GameObject objCameraPlayer = GameObject.FindGameObjectWithTag("MainCamera");
            if (objCameraPlayer != null)
            {
                cameraPlayer = objCameraPlayer.GetComponent<Camera>();
                canvasFundoVideo.SetActive(false);
                if (iniciouCorrida || videoPlayer == null)
                {
                    cameraPlayer.enabled = true;
                }
            }
        }
    }

    [PunRPC]
    void RPC_IniciarContagemRegressiva()
    {
        iniciouContagem = true;
        if(videoPlayer != null) videoPlayer.Stop();
        if(cameraPlayer != null) cameraPlayer.enabled = true;
    }

    [PunRPC]
    void RPC_IniciarCorrida(float cronometro)
    {
        if (iniciouCorrida) return;
        cronometroPartida = cronometro;
        txContagemLargada.gameObject.SetActive(false);
        iniciouCorrida = true;
        txCronometro.gameObject.SetActive(true);
        paredeLargada.SetActive(false);
        abrirPortaoD.Play();
        abrirPortaoE.Play();
        setarCameraVideoPlayer();
        Debug.Log("Iniciou corrida" );
    }

    public void PreencherJogadoresDaCorrida(string nomeVencedor, int cronometro)
    {
        pausarCronometro = true;
		this.primeiroColocado = new ColocacaoPlayer(nomeVencedor, "1");
		Debug.Log("O vencedor � o player: " + primeiroColocado.nome + " " + primeiroColocado.colocao);
		PlayerPrefs.SetString("1col", primeiroColocado.nome);
        PlayerPrefs.SetInt("1coltime", cronometro);
    }

    public void EnviarInformacoesDaCorrida()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PV.RPC("RPC_IniciarCorrida", RpcTarget.All, cronometroPartida);
        }
    }

    public void AbrirHudScoreBunnyhop()
    {
        pausarCronometro = true;
        canvasScoreBunnyhop.SetActive(true);
        txScoreBunnyhop.text = "Score: " + (int)score + "";
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    public void FecharHudScoreBunnyhop()
    {
        canvasScoreBunnyhop.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void PlayAgainModoBunnyhop()
    {
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        objPlayer.GetComponent<PlayerController>().playerManager.Die();
        IniciarVariaveisDaCorrida();
        FecharHudScoreBunnyhop();
    }

}
